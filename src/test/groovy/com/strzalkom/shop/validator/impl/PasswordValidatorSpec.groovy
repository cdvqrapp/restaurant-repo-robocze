package com.strzalkom.shop.validator.impl

import com.strzalkom.shop.model.dto.UserDto
import spock.lang.Specification

class PasswordValidatorSpec extends Specification {
    def passwordValidator = new PasswordValidator()

    def 'Should test password validator'() {
        given:
        def userDto = new UserDto(password: password, confirmPassword: confirmPassword)

        when:
        def result = passwordValidator.isValid(userDto, null)

        then:
        result == expected //jak equals

        where:
        password | confirmPassword || expected
        "abc"    | "abc"           || true
        "abb"    | "a"             || false



    }


}
