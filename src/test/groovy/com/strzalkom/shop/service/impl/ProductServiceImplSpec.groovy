package com.strzalkom.shop.service.impl

import com.strzalkom.shop.config.properties.FileConfig
import com.strzalkom.shop.helper.FileHelper
import com.strzalkom.shop.model.dao.Product
import com.strzalkom.shop.model.dao.User
import com.strzalkom.shop.repository.ProductRepository
import org.apache.commons.io.FilenameUtils
import org.springframework.web.multipart.MultipartFile
import org.xmlunit.builder.Input
import spock.lang.Specification

import java.nio.file.Path
import java.nio.file.Paths

class ProductServiceImplSpec extends Specification {

    def productRepository = Mock(ProductRepository)
    def fileConfig = Mock(FileConfig)
    def fileHelper = Mock(FileHelper)
    def productServiceImpl = new ProductServiceImpl(productRepository, fileConfig, fileHelper)


    def 'should get product by ID'() {
        given:
        def id = 10

        when:
        productServiceImpl.findById(id)

        then:
        1 * productRepository.getById(id)
        0 * _
    }

    def 'should save product'() {
        given:
        def product = Mock(Product)
        def image = Mock(MultipartFile)
        def imagePath = "C/images/13.pdf"
        def inputStream = Mock(InputStream)

        when:
        productServiceImpl.save(product, image)

        then:
        1 * productRepository.save(product)
        //multipart file upload pliku w REST
        1 * image.getOriginalFilename() >> "abcd.pdf"
        1 * fileConfig.getProductPath() >> "C/images/"
        1 * product.getId() >> 13
        1 * image.getInputStream() >> inputStream
        1 * fileHelper.saveFile(inputStream, imagePath)
        1 * product.setImagePath(imagePath)
        0 * _

    }

    def 'should upadate product'() {


        given:
        def product = Mock(Product)
        def id = 10;
        def image = Mock(MultipartFile)
        def imagePath = "C/images/10.pdf"
        def inputStream = Mock(InputStream)

        when:
        def result = productServiceImpl.update(product, id, image)

        then:
        1 * productRepository.getById(id) >> new Product(id: id)
        1 * product.getName() >> "Makaron"
        1 * product.getQuantity() >> 2
        1 * product.getPrice() >> 200
        1 * product.getDescription() >> "To jest makaron"
        1 * product.getCode() >> "MAK"
        1 * image.getOriginalFilename() >> "abcd.pdf"
        1 * fileConfig.getProductPath() >> "C/images/"
        1 * product.getId() >> id
        1 * image.getInputStream() >> inputStream
        1 * fileHelper.saveFile(inputStream, imagePath)
        1 * product.setImagePath(imagePath)
        0 * _

//        1 * product.getName() >> "Makaron"
//        1 * product.getQuantity() >> 2
//        1 * product.getPrice() >> 200
//        1 * product.getDescription() >> "To jest makaron"
//        1 * product.getCode() >> "MAK"
//        0 * _

        result.id == 10
        result.name == "Makaron"
        result.quantity == 2
        result.price == 200
        result.description == "To jest makaron"
        result.code == "MAK"
        result.imagePath == "C/images/10.pdf"
    }

    def 'should delete product'() {
        given:
        def id = 13
        def product = Mock(Product)


        when:
        productServiceImpl.deleteById(id)

        then:
        1 * productRepository.getById(id) >> product
        1 * product.getImagePath() >> "abcd"
        1 * fileHelper.deleteFile("abcd")
        1 * productRepository.deleteById(id)
        0 * _


    }

}
