package com.strzalkom.shop.service.impl

import com.strzalkom.shop.exception.QuantityExcededException
import com.strzalkom.shop.model.dao.Basket
import com.strzalkom.shop.model.dao.Product
import com.strzalkom.shop.model.dao.User
import com.strzalkom.shop.repository.BasketRepository
import com.strzalkom.shop.service.ProductService
import com.strzalkom.shop.service.UserService
import spock.lang.Specification

class BasketServiceImplSpec extends Specification {


    def basketRepository = Mock(BasketRepository)
    def userService = Mock(UserService)
    def productService = Mock(ProductService)

    def basketServiceImpl = new BasketServiceImpl(basketRepository, productService, userService)

    def 'should not add product to basket - when quantity is exceded, product exist in basket'() {
        given:
        def productId = 1
        def quantity = 3
        def user = Mock(User)
        def userId = 2
        def basket = Mock(Basket)
        def product = Mock(Product)


        when:
        basketServiceImpl.updateBasket(productId, quantity)

        then:
        1 * userService.findCurrentUser() >> user
        1 * user.getId() >> userId
        1 * basketRepository.findByUserIdAndProductId(userId, productId) >> Optional.of(basket)
        2 * basket.getProduct() >> product
        1 * product.getQuantity() >> 4
        1 * basket.getQuantity() >> 2
        1 * product.getName() >> "Frytki"
        0 * _

        def e = thrown QuantityExcededException
        e.message == "Wrong quantity for product Frytki During updating basket"


    }

    def 'should  add product to basket - when product exist in basket'() {
        given:
        def productId = 1
        def quantity = 3
        def user = Mock(User)
        def userId = 2
        def basket = Mock(Basket)
        def product = Mock(Product)


        when:
        basketServiceImpl.updateBasket(productId, quantity)

        then:
        1 * userService.findCurrentUser() >> user
        1 * user.getId() >> userId
        1 * basketRepository.findByUserIdAndProductId(userId, productId) >> Optional.of(basket)
        1 * basket.getProduct() >> product
        1 * product.getQuantity() >> 7
        2 * basket.getQuantity() >> 1
        1 * basket.setQuantity(4)
        1 * basketRepository.save(basket)
        0 * _
    }

    def 'should  not add product to basket - when product doesn t exist in basket and quantity is exceded '() {
        given:
        def productId = 1
        def quantity = 3
        def user = Mock(User)
        def userId = 2
        def basket = Mock(Basket)
        def product = Mock(Product)


        when:
        basketServiceImpl.updateBasket(productId, quantity)

        then:
        1 * userService.findCurrentUser() >> user
        1 * user.getId() >> userId
        1 * basketRepository.findByUserIdAndProductId(userId, productId) >> Optional.empty()
        1 * productService.findById(productId) >> product
        1 * product.getQuantity() >> 1
        1 * product.getName() >> "Frytki"
        0 * _

        def e = thrown QuantityExcededException
        e.message == "Wrong quantity for product Frytki During Added product to basket"

    }

    def 'should  not add product to basket - when product doesn t exist in basket and quantity is ok '() {
        given:
        def productId = 1
        def quantity = 3
        def user = Mock(User)
        def userId = 2
        def product = Mock(Product)
        def basket = Basket.builder()
                .user(user)
                .product(product)
                .quantity(quantity)
                .build()


        when:
        basketServiceImpl.updateBasket(productId, quantity)

        then:
        1 * userService.findCurrentUser() >> user
        1 * user.getId() >> userId
        1 * basketRepository.findByUserIdAndProductId(userId, productId) >> Optional.empty()
        1 * productService.findById(productId) >> product
        1 * product.getQuantity() >> 5
        1 * basketRepository.save(basket)
        0 * _

    }
}