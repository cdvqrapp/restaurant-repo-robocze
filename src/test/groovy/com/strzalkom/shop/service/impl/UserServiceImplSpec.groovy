package com.strzalkom.shop.service.impl

import com.strzalkom.shop.model.dao.User
import com.strzalkom.shop.repository.RoleRepository
import com.strzalkom.shop.repository.UserRepository
import org.springframework.security.core.Authentication
import org.springframework.security.core.context.SecurityContext
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.crypto.password.PasswordEncoder
import spock.lang.Specification

class UserServiceImplSpec extends Specification {

    def userRepository = Mock(UserRepository)
    def roleRepository = Mock(RoleRepository)
    def passwordEncoder = Mock(PasswordEncoder)

    def userServiceImpl = new UserServiceImpl(userRepository, roleRepository, passwordEncoder)


    def 'should get user by ID'() {
        given:
        def id = 12;

        when:
        userServiceImpl.findById(id)

        then:
        1 * userRepository.getById(id)  //sprawdzam czy metoda findById wywolala sie 1 raz z konkretna wartoscia
        0 * _ //nic wiecej juz sie nie wykonalo

    }

    def ' should update user'() {
        given:
        def user = Mock(User)
        def id = 36;

        when:
        def result = userServiceImpl.update(user, id)

        then:
        1 * userRepository.getById(id) >> new User(id: id)
        1 * user.getEmail() >> "test@test.pl"
        1 * user.getFirstName() >> "Test"
        1 * user.getLastName() >> "Testowy"
        0 * _

        result.id == 36
        result.email == "test@test.pl"
        result.firstName == "Test"
        result.lastName == "Testowy"
// mix - poprawne wykonanie metod + wartosc zwracana
    }
//    def ' should find current user '() {
//        given:
//        def securityContext = Mock(SecurityContext)
//        SecurityContextHolder.setContext(securityContext) //wywolanie metody statycznej
//        def authentication = Mock(Authentication)
//
//
//        when:
//        userServiceImpl.findCurrentUser()
//
//        then:
//        1 * securityContext.getAuthentication() >> authentication
//        1 * authentication.getName() >> "test@test.pl"
//        1 * userRepository.findByEmail("test@test.pl") >> // optional
//        1 *
//        0 * _
//
//
//    }
    }
