package com.strzalkom.shop.mapper

import com.strzalkom.shop.model.dao.Product
import com.strzalkom.shop.model.dto.ProductDto
import spock.lang.Specification

class ProductMapperImplSpec extends Specification {

    def productMapper = new ProductMapperImpl()

    def 'Should test product to productDto mapper'() {
        given:
        def product = Product.builder()
                .id(12)
                .name("Makaron")
                .quantity(2)
                .description("to jest makaron")
                .price(20)
                .code("MAK")
                .build()

        when:
        def result = productMapper.productToProductDto(product)

        then:
        result.id == product.id
        result.name == product.name
        result.quantity == product.quantity
        result.description == product.description
        result.price == product.price
        result.code == product.code
        result.revisionNumber == null
    }

    def 'Should test productDto to product mapper'() {
        given:
        def productDto = ProductDto.builder()
                .id(12)
                .name("Tosty")
                .quantity(4)
                .description("to sa tosty")
                .price(20)
                .code("TOS")
                .revisionNumber(null)
                .build()

        when:
        def result = productMapper.productDtoToProduct(productDto)

        then:
        result.id == productDto.id
        result.name == productDto.name
        result.quantity == productDto.quantity
        result.description == productDto.description
        result.price == productDto.price
        result.code == productDto.code

    }
}
