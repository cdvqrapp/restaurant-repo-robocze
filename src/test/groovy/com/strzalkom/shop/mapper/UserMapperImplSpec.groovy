package com.strzalkom.shop.mapper

import com.strzalkom.shop.model.dao.User
import com.strzalkom.shop.model.dto.UserDto
import spock.lang.Specification

import java.time.LocalDateTime

class UserMapperImplSpec extends Specification {

    def userMapper = new UserMapperImpl()

    def 'Should test user to userDto mapper'() {
        given:
        def user = User.builder()
                .id(88)
                .firstName("Adam")
                .lastName("Testowy")
                .password("123")
                .email("adam@testowy")
                .createdBy("anonymousUser")
                .createdDate(LocalDateTime.of(2020, 1, 1, 1, 1, 1))
                .lastModifiedBy(null)
                .lastModifiedDate(null)
                .roles(null)
                .build()


        when:
        def result = userMapper.userToUserDto(user)

        then:
        result.id == user.id
        result.firstName == user.firstName
        result.lastName == user.lastName
        result.email == user.email
        result.password == null
        result.confirmPassword == null
        result.revisionNumber == null

    }

    def 'Should test userDto to user mapper'() {
        given:
        def userDto = UserDto.builder()
                .id(88)
                .firstName("Ania")
                .lastName("Testowa")
                .email("ania@testowa.pl")
                .password("123")
                .confirmPassword("123")
                .revisionNumber(10)
                .build()

        when:
        def result = userMapper.userDtoToUser(userDto)

        then:
        result.id == userDto.id
        result.firstName == userDto.firstName
        result.lastName == userDto.lastName
        result.email == userDto.email
        result.password == userDto.password

    }


}
