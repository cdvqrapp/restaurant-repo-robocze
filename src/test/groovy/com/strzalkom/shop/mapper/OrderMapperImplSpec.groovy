package com.strzalkom.shop.mapper

import com.strzalkom.shop.model.dao.Order
import com.strzalkom.shop.model.dao.OrderDetails
import com.strzalkom.shop.model.dao.Product
import com.strzalkom.shop.model.dto.OrderDto
import spock.lang.Specification

class OrderMapperImplSpec extends Specification {

    def orderMapper = new OrderMapperImpl()
    def product = Mock(Product)
    def orderDetails = Mock(OrderDetails)
    def order = Mock(Order)

    def 'Should test order to orderDto mapper'() {

        given:


        def order = Order.builder()
                .id(2)
                .product(product)
                .orderDetails(orderDetails)
                .quantity(1)
                .build()

        when:
        def result = orderMapper.orderToOrderDto(order)

        then:
        result.productId == product.id
        result.orderDetails == order.orderDetails
        result.quantity == order.quantity

    }


}

