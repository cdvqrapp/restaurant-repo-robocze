package com.strzalkom.shop.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.strzalkom.shop.model.dto.ProductDto;
import com.strzalkom.shop.repository.ProductRepository;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.runner.Request;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.multipart;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest     // uruchamia testy dla springa
@AutoConfigureMockMvc // requesty na controller
@ActiveProfiles("test") // profil springowy
@TestPropertySource(locations = "classpath:application-test.yml")
class ProductControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    @WithMockUser(authorities = "SCOPE_ADMIN")
    @DisplayName("User should save product")
    void shouldSaveProductByAdmin() throws Exception {
        MockMultipartFile product = new MockMultipartFile("product", objectMapper.writeValueAsBytes(ProductDto.builder()
                .name("Test")
                .quantity(2)
                .description("Testowy opis")
                .price(20L)
                .code("TST")
                .build()));
        MockMultipartFile image = new MockMultipartFile("image", "test.jpg", MediaType.IMAGE_JPEG_VALUE, new byte[0]); //APPLICATION_OCTET_STREAM_VALUE - uniwersalny do plikow mediatype
        mockMvc.perform(multipart("/api/products")
                .file(product)
                .file(image)
                .with(request -> {
                    request.setMethod("POST");
                    return request;
                }))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").exists())
                .andExpect(jsonPath("$.name").value("Test"))
                .andExpect(jsonPath("$.quantity").value(2))
                .andExpect(jsonPath("$.description").value("Testowy opis"))
                .andExpect(jsonPath("$.price").value(20L))
                .andExpect(jsonPath("$.imagePath").value("/Users/mateuszstrzalko/photos/1.jpg"))
                .andExpect(jsonPath("$.code").value("TST"));//with przyjmuje interfejs funkcyjny unaryOperator ten sam typ co przyjmuje to ten sam zwracam

//    .andExpect(jsonPath("$.firstName").value("Adam"))

    }


    @Test
    @WithMockUser(roles = "ADMIN")
    @DisplayName("User should delete product")
    void shouldDeleteProductByAdmin() throws Exception {

    }


}
