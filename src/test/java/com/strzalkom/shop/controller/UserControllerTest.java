package com.strzalkom.shop.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.strzalkom.shop.model.dao.User;
import com.strzalkom.shop.model.dto.UserDto;
import com.strzalkom.shop.repository.UserRepository;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.servlet.MockMvc;

import javax.transaction.Transactional;

import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest     // uruchamia testy dla springa
@Transactional // dzieki temu rollback wszystkich zmian wprowadzonych zmian podczastestow ( po wykonaniu metody testowej)
@AutoConfigureMockMvc // requesty na controller
@ActiveProfiles("test") // profil springowy
@TestPropertySource(locations = "classpath:application-test.yml")
class UserControllerTest {
    //konstrukcja autowired do wstrzykniecia nie konstruktor
//JUNIT5 + java
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private UserRepository userRepository;


    @Test
    void shouldSaveUser() throws Exception {
        mockMvc.perform(post("/api/users")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(UserDto.builder()
                                .email("adam@adamski")
                                .firstName("Adam")
                                .lastName("Adamski")
                                .password("q1w2e3r4")
                                .confirmPassword("q1w2e3r4")
                                .build())))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.firstName").value("Adam"))
                .andExpect(jsonPath("$.lastName").value("Adamski"))
                .andExpect(jsonPath("$.password").doesNotExist())
                .andExpect(jsonPath("$.confirmPassword").doesNotExist())
                .andExpect(jsonPath("$.email").value("adam@adamski"))
                .andExpect(jsonPath("$.id").exists());
    }

    @Test
    void shouldNotSaveUser() throws Exception {
        mockMvc.perform(post("/api/users")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(UserDto.builder()
                                .email("adamadamski")
                                .firstName("     ")
                                .password("q1w2e3r4")
                                .confirmPassword("q1w2e3r4")
                                .build())))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$[*].message", containsInAnyOrder("must not be blank",
                        "must be a well-formed email address", "must not be blank")))
                .andExpect(jsonPath("$[*].field", containsInAnyOrder("lastName", "email", "firstName")));
    }

    @Test
    @DisplayName("Not authorized user try to delete other user")
    void shouldNotDeleteUserByNotAuthenticatedUser() throws Exception {
        mockMvc.perform(delete("/api/users/40"))
                .andExpect(status().isUnauthorized())
                .andExpect(jsonPath("$").doesNotExist());
    }

    @Test
    @WithMockUser(authorities = "SCOPE_USER") //mockuje usera z rola user
    @DisplayName("Authorized user but not admin try to delete other user")
    void shouldNotDeleteUserByNotAdminUser() throws Exception {
        mockMvc.perform(delete("/api/users/31222"))
                .andExpect(status().isForbidden())
                .andExpect(jsonPath("$").doesNotExist());
    }

    @Test
    @WithMockUser(authorities = "SCOPE_ADMIN")
    @DisplayName("Admin user try to delete non existing user")
    void shouldNotDeleteNonExistingUserByAdmin() throws Exception {
        mockMvc.perform(delete("/api/users/9999"))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$").doesNotExist());
    }

    @Test
    @WithMockUser(authorities = "SCOPE_ADMIN")
    @DisplayName("Admin user try to delete user")
    void shouldDeleteUserByAdmin() throws Exception {
        User user = userRepository.save(User.builder()
                .email("adamadamski")
                .firstName("adam")
                .password("q1w2e3r4")
                .build());
        mockMvc.perform(delete("/api/users/" + user.getId()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").doesNotExist());
    }

    @Test
    @WithMockUser(authorities = "SCOPE_USER")
    @DisplayName("User try to update himself")
    void shouldUpdateExisingUserByUser() throws Exception {
        User user = userRepository.save(User.builder()
                .email("user")
                .firstName("adam")
                .password("q1w2e3r4")
                .build());
        mockMvc.perform(put("/api/users/" + user.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(UserDto.builder()
                                .email("adam@adamski")
                                .firstName("LUCJAN")
                                .lastName("Adamski")
                                .password("q1w2e3r4")
                                .confirmPassword("q1w2e3r4")
                                .build())))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.firstName").value("LUCJAN"))
                .andExpect(jsonPath("$.lastName").value("Adamski"))
                .andExpect(jsonPath("$.password").doesNotExist())
                .andExpect(jsonPath("$.confirmPassword").doesNotExist())
                .andExpect(jsonPath("$.email").value("adam@adamski"))
                .andExpect(jsonPath("$.id").exists());

    }

    @Test
    @WithMockUser(authorities = "SCOPE_USER")
    @DisplayName("Authorized user try to update other user")
    void shouldNotUpdateExistingUserByUser() throws Exception {
        userRepository.save(User.builder()
                .email("user")
                .firstName("adam")
                .password("q1w2e3r4")
                .build());
        User user = userRepository.save(User.builder()
                .email("adam@update")
                .firstName("Piotr")
                .password("q1w2e3r4")
                .build());
        mockMvc.perform(put("/api/users/" + user.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(UserDto.builder()
                                .email("adam@adamski")
                                .firstName("LUCJAN")
                                .lastName("Adamski")
                                .password("q1w2e3r4")
                                .confirmPassword("q1w2e3r4")
                                .build())))
                .andExpect(status().isForbidden())
                .andExpect(jsonPath("$").doesNotExist());

    }

    @Test
    @WithMockUser(authorities = "SCOPE_ADMIN")
    @DisplayName("Admin user try to update non existing user")
    void shouldNotUpdateNonExistingUserByAdmin() throws Exception {
        mockMvc.perform(put("/api/users/99999")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(UserDto.builder()
                                .email("adam@adamski")
                                .firstName("LUCJAN")
                                .lastName("Adamski")
                                .password("q1w2e3r4")
                                .confirmPassword("q1w2e3r4")
                                .build())))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$").doesNotExist());

    }

    @Test
    @WithMockUser(authorities = "SCOPE_ADMIN")
    @DisplayName("Admin user try to update existed user")
    void shouldUpdateUserByAdmin() throws Exception {
        User user = userRepository.save(User.builder()
                .email("adamadamski")
                .firstName("adam")
                .password("q1w2e3r4")
                .build());
        mockMvc.perform(put("/api/users/" + user.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(UserDto.builder()
                                .email("adam@adamski")
                                .firstName("LUCJAN")
                                .lastName("Adamski")
                                .password("q1w2e3r4")
                                .confirmPassword("q1w2e3r4")
                                .build())))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.firstName").value("LUCJAN"))
                .andExpect(jsonPath("$.lastName").value("Adamski"))
                .andExpect(jsonPath("$.password").doesNotExist())
                .andExpect(jsonPath("$.confirmPassword").doesNotExist())
                .andExpect(jsonPath("$.email").value("adam@adamski"))
                .andExpect(jsonPath("$.id").value(user.getId()));
    }

    @Test
//    @WithMockUser(authorities = "SCOPE_USER")
    @DisplayName("User try to get existing user")
    void shouldNotGetUserByOtherUser() throws Exception {
        User user = userRepository.save(User.builder()
                .email("adamadamski")
                .firstName("adam")
                .password("q1w2e3r4")
                .build());
        mockMvc.perform(get("/api/users/" + user.getId()))
                .andExpect(status().isUnauthorized())
                .andExpect(jsonPath("$").doesNotExist());

    }

    @Test
    @WithMockUser(authorities = "SCOPE_ADMIN")
    @DisplayName("Admin try to get existing user")
    void shouldGetUserByAdmin() throws Exception {
        User user = userRepository.save(User.builder()
                .email("adam@adamski")
                .firstName("adam")
                .password("q1w2e3r4")
                .build());
        mockMvc.perform(get("/api/users/" + user.getId()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.firstName").value("adam"))
                .andExpect(jsonPath("$.password").doesNotExist())
                .andExpect(jsonPath("$.confirmPassword").doesNotExist())
                .andExpect(jsonPath("$.email").value("adam@adamski"))
                .andExpect(jsonPath("$.id").exists());

    }

    @Test
    @WithMockUser(authorities = "SCOPE_ADMIN")
    @DisplayName("Admin try to get nonexisting user")
    void shouldNotGetUserByAdmin() throws Exception {
        userRepository.save(User.builder()
                .email("adam@adamski")
                .firstName("adam")
                .password("q1w2e3r4")
                .build());
        mockMvc.perform(get("/api/users/999999"))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$").doesNotExist());

    }


    @Test
    @WithMockUser(authorities = "SCOPE_USER")
    @DisplayName("User try to get page of users ")
    void shouldNotGetUsersPageUsingUserRole() throws Exception {
        mockMvc.perform(get("/api/users/")
                        .param("page", "0")
                        .param("size", "3"))
                .andExpect(status().isForbidden())
                .andExpect(jsonPath("$").doesNotExist());
    }

    @Test
    @WithMockUser(authorities = "SCOPE_ADMIN")
    @DisplayName("Get users page by admin")
    void shouldGetUsersPageByAdmin() throws Exception {
        userRepository.save(User.builder()
                .email("adamadamski")
                .firstName("adam")
                .password("q1w2e3r4")
                .build());
        userRepository.save(User.builder()
                .email("lucjan")
                .firstName("adam")
                .password("q1w2e3r4")
                .build());
        userRepository.save(User.builder()
                .email("test")
                .firstName("adam")
                .password("q1w2e3r4")
                .build());
        mockMvc.perform(get("/api/users/")
                        .param("page", "0")
                        .param("size", "3"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content.length()").value(3));
    }


}
