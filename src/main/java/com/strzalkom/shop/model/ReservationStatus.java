package com.strzalkom.shop.model;

public enum ReservationStatus {
    FREE,
    RESERVATION,
    WAITING
}
