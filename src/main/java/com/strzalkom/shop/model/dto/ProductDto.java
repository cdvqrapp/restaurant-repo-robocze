package com.strzalkom.shop.model.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.strzalkom.shop.model.dao.Category;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Positive;
import javax.validation.constraints.PositiveOrZero;

@Data
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL) //gdy zmienna null to nie zostanie dodana do JSONa
@NoArgsConstructor
@AllArgsConstructor

public class ProductDto {
    private Long id;

    @NotBlank
    private String name;

    @PositiveOrZero
    private Integer quantity;
    @NotBlank
    private String description;

    @Min(value = 0, message = "The price should not be less than 0")
    private Long price;
    //adnotacja nie mniejsza niż 0
    @NotBlank
    private String code;

    private Integer revisionNumber;

    private String imagePath;

    private CategoryDto category;


}
