package com.strzalkom.shop.model.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL) //gdy zmienna null to nie zostanie dodana do JSONa
@NoArgsConstructor
@AllArgsConstructor
public class FieldErrorDto {


    private String message;

    private String field;


}
