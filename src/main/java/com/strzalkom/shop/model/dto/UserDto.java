package com.strzalkom.shop.model.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.strzalkom.shop.validator.PasswordValid;
import com.strzalkom.shop.validator.group.Create;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import java.util.List;
import java.util.UUID;


@Data
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL) //gdy zmienna null to nie zostanie dodana do JSONa
@NoArgsConstructor
@AllArgsConstructor
@PasswordValid(groups = Create.class)

//czy w dto jest potrzebny AllArgsConstructor // czy w dao
public class UserDto {

    private Long id;
    // trzy możliwe adnotacje notnull - wszystkie typy / (notempty- długość stringa większa niż 0  notblank - długość stringa po usunięciu wszystkich spacji jest dłuższa niż 0) - tylko stringi

    @NotBlank
    private String firstName;
    @NotBlank
    private String lastName;
    @Email
    @NotBlank
    private String email;
    @NotBlank
   // @Pattern(regexp = "A-Z")  - POCZYTAJ REGEX - zrobic walidator dla hasła
    private String password;

    private List<String> roles;

    private String confirmPassword;

    private Integer revisionNumber;


}
