package com.strzalkom.shop.model.dto;

import com.strzalkom.shop.model.dao.Order;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TableDto {


    private Long id;

    private Integer guestCounter;

    @NotBlank
    private Long tableNumber;

    private String description;


    //jak zrobic w controller i w serwisie zwracanie id zamowienia
}
