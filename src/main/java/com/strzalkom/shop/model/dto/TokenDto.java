package com.strzalkom.shop.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
@Data
@NoArgsConstructor
@AllArgsConstructor
public class TokenDto {

    private String token;
}
