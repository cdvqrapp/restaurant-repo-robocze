package com.strzalkom.shop.model.dto;

import com.strzalkom.shop.model.dao.OrderDetails;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Positive;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class OrderDto {

    private Long productId;
    private OrderDetails orderDetails;
    @Positive(message = "The quantity should not be less than 0")
    private Integer quantity;

    private Integer revisionNumber;

    private Long unitPrice;
}


