package com.strzalkom.shop.model.dto;

import com.strzalkom.shop.model.OrderStatus;
import com.strzalkom.shop.model.PaymentMethod;
import com.strzalkom.shop.model.dao.TableObject;
import com.strzalkom.shop.model.dao.User;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.Currency;
import java.util.Date;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class OrderDetailsDto {

    private Long id;

    private OrderStatus orderStatus;

    private LocalDateTime createdDate;

    private String clientDescription;

    private Long totalPrice;

    private Currency currency;

    private PaymentMethod paymentMethod;


    private Long tableObjectId;

    private Long tableNumber;

    private List<ProductDto> productDtoList;
//problem z relacjami
}
