package com.strzalkom.shop.model.dao;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Reservation {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    private Order order;

    @ManyToOne
    private TableObject tableObject;

    @ManyToOne
    private User user;

    private String description;

    private LocalDateTime reservationFrom;

    private LocalDateTime reservationTo;






    //id user, order , table, status, description, godziny od do
}
