package com.strzalkom.shop.model.dao;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data   //generuje kod dodatkowy gettery, settery, toString, equals i hashcode
@Entity //encja bazodanowa
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Basket {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY) //AUTO/STRATEGY - identity dodaje auto increment, sequence dodaje sekwencje
    private Long id;
    @ManyToOne
    private User user;
    @ManyToOne
    private Product product;


    private Integer quantity;
}
