package com.strzalkom.shop.model.dao;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.persistence.Table;

@Data   //generuje kod dodatkowy gettery, settery, toString, equals i hashcode
@Entity //encja bazodanowa
@Builder
@Table(name = "orders")
@AllArgsConstructor
@NoArgsConstructor
public class Order{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    //AUTO/STRATEGY - identity dodaje auto increment, sequence dodaje sekwencje
    private Long id;

    @ManyToOne
    private Product product;
    @OneToOne
    private OrderDetails orderDetails;

    private Integer quantity;

    private Long unitPrice;


}
