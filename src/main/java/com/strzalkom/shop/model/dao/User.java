package com.strzalkom.shop.model.dao;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Type;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@Entity //opisuje tabelkę bazodanową, generuje schemat bazy danych
@Builder(toBuilder = true)// wzorzec projektowy builder generuje // to budiler tworzy buildera na obiekcie z wartoscami tego obiektu
@Data
//lombok  (adnotacja @Data) generuje kod  generuje gettety i setter, equals, hashcode i toString, wieloargumentowy konstruktor dla finalnych zmiennych
//@Getter - getter, @Setter - setter, @EqualsAndHashCode - zawsze razem, @ToString, @RequiredArgsConstructor
@Audited //generuje tablke na podstawie usera User_Aud (audyt) historia zmian na userze
@EntityListeners(AuditingEntityListener.class) //defaultowy audyt  auto wstawianie wartosci do pol createdby itd
@NoArgsConstructor //generuje bez argumentowy konstruktor, setterami dodaje pola
@AllArgsConstructor // wieloargumentowy konstruktor (dla niefinalnych zmiennych) - dla buildera

public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY) //Spring generuje id jako autoinkrement
    private Long id; //korzystanie z JPA dlatego z dużej
    // Klasy są zawsze POJO - wszystkie pola są prywatne, dostęp do nich przez gettery i settery, nadpisanie metod z klasy object, konstruktory bezargumentowe lub wieloargumentowe
    //defaultowo to package
    private String firstName;
    private String lastName;

    @Column(unique = true, nullable = false)  //zmienna w bazie danych bedzie unikalna
    private String email;


    @NotAudited //nie przechowywac hasla
    private String password;

    @CreatedDate
    private LocalDateTime createdDate;              //rok, miesiac, dzien, godzina, minuta, milisekunda                 ZoneDateTime - strefa czasowa
    @LastModifiedDate
    private LocalDateTime lastModifiedDate;
    @CreatedBy
    private String createdBy;
    @LastModifiedBy
    private String lastModifiedBy;

    @ManyToMany//(fetch = FetchType.EAGER) //lazy i eager  (z defaultu lazy) = gdy pobieramy obiekt z db to nie pobierają się relacja pobiera jak wywołuje się gettera, eager pobiera usera z rolami
    @JoinTable(name = "user_role", inverseJoinColumns = @JoinColumn(name = "role_id"))
    private List<Role> roles;

    @Type(type = "org.hibernate.type.UUIDCharType")
    private UUID activatedToken;

}
