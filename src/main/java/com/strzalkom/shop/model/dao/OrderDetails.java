package com.strzalkom.shop.model.dao;

import com.strzalkom.shop.model.OrderStatus;
import com.strzalkom.shop.model.PaymentMethod;
import com.strzalkom.shop.model.dto.ProductDto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Currency;
import java.util.List;

@Data   //generuje kod dodatkowy gettery, settery, toString, equals i hashcode
@Entity // encja bazodanowa
@Builder
@AllArgsConstructor
@NoArgsConstructor
@EntityListeners(AuditingEntityListener.class)
public class OrderDetails {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    private User user;
    @Enumerated(EnumType.STRING) //w db jako string
    private OrderStatus orderStatus;

    @CreatedDate
    private LocalDateTime createdDate;

    private String clientDescription;

    private Long totalPrice;

    private Currency currency;

    private PaymentMethod paymentMethod;

//do poprawy bo duplikat
    @ManyToOne
    @JoinColumn(name = "table_id")
    private TableObject tableObject;

    @ManyToOne
    @JoinColumn(name = "table_number")
    private TableObject tableObjectNumber;

}
