package com.strzalkom.shop.model.dao;

import lombok.AllArgsConstructor;
import lombok.Data;

import lombok.NoArgsConstructor;
import org.hibernate.envers.Audited;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

//final - modyfikator wlaściwości (klasa)- nie można dziedziczyć po klasie
// private final Lista = nie mogę przypisywać nowej wartości ale mogę dodawać i usuwać z listy elementy
//metoda finalna to taka która nie może zostać nadpisana
// static i final w parze zazwyczaj

@Audited
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor

public  class Role {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;

}
