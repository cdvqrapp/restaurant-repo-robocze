package com.strzalkom.shop.model.dao;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Entity
@Builder
@Table(indexes = @Index(name = "idx_name", columnList = "name", unique = true))
@NoArgsConstructor
@AllArgsConstructor
public class Template {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    //indeks przyspiesza wyszukiwanie danych po danej kolumnie w indeksie ale spowalnia aktualizacje

    private String name;

    private String subject;

    private String body;
}
