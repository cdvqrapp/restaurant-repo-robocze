package com.strzalkom.shop.model.dao;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@Builder(toBuilder = true) //dodaje mozliwosc uzycia metody toBuilder
@NoArgsConstructor
@AllArgsConstructor
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;


    private String name;
    private Integer quantity;
    private String description;
    private Long price;
    private String code;
    private String imagePath;

    @ManyToOne
    private Category category;


}
