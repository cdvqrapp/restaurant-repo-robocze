package com.strzalkom.shop.controller;

import com.strzalkom.shop.mapper.TableMapper;
import com.strzalkom.shop.model.dao.TableObject;
import com.strzalkom.shop.model.dto.TableDto;
import com.strzalkom.shop.service.TableService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/table")//prefix w linku do każdego endpointu
@RequiredArgsConstructor
public class TableController {
    private final TableService tableService;
    private final TableMapper tableMapper;

    @PostMapping
    @PreAuthorize("hasAuthority('SCOPE_ADMIN')")
    @Operation(security = @SecurityRequirement(name = "BearerToken"))
    public TableDto saveTable(@RequestBody TableDto table) {
        return tableMapper.tableToTableDto(tableService.save(tableMapper.tableDtoToTable(table)));
    }

    @GetMapping("/{id}")
    @PreAuthorize("hasAuthority('SCOPE_ADMIN')")
    @Operation(security = @SecurityRequirement(name = "BearerToken"))
    public TableDto getTable(@PathVariable Long id) {
        TableObject tableObject = tableService.findById(id);
        return tableMapper.tableToTableDto(tableObject);
    }

    @PutMapping("/{id}")
    @PreAuthorize("hasAuthority('SCOPE_ADMIN')")
    @Operation(security = @SecurityRequirement(name = "BearerToken"))
    public TableDto updateTable(@PathVariable Long id, @RequestBody TableDto tableDto) {
        return tableMapper.tableToTableDto(tableService.update(tableMapper.tableDtoToTable(tableDto), id));
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasAuthority('SCOPE_ADMIN')")
    @Operation(security = @SecurityRequirement(name = "BearerToken"))
    public void deleteTable(@PathVariable Long id) {
        tableService.deleteById(id);
    }

    @GetMapping
    @PreAuthorize("hasAuthority('SCOPE_ADMIN')")
    @Operation(security = @SecurityRequirement(name = "BearerToken"))
    public Page<TableDto> getTablePage(@RequestParam int page, @RequestParam int size) {
        return tableService.getPage(PageRequest.of(page, size)).map(tableMapper::tableToTableDto);

    }
}


