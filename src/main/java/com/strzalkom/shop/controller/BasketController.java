package com.strzalkom.shop.controller;

import com.strzalkom.shop.mapper.ProductMapper;
import com.strzalkom.shop.model.dto.BasketDto;
import com.strzalkom.shop.model.dto.ProductDto;
import com.strzalkom.shop.service.BasketService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Validated
@RestController
@RequestMapping("/api/baskets")
@RequiredArgsConstructor
public class BasketController {

    private final BasketService basketService;
    private final ProductMapper productMapper;

    //deklaracja nie jest rownoznaczna z wstrzyknieciem , private final to wstrzykniecie
//2 rodzaje wstrzykiwania w springu, autowired(uzywane w testach integracyjnych), przez konstruktor, to jest przez konstruktor
    @PutMapping
    @PreAuthorize("isAuthenticated()")
    @Operation(security = @SecurityRequirement(name = "BearerToken"))
    public void updateBasket(@RequestBody @Valid BasketDto basketDto) {
        basketService.updateBasket(basketDto.getProductId(), basketDto.getQuantity());

        //requestbody dwa parametry
    }

    @GetMapping
    @PreAuthorize("isAuthenticated()")
    @Operation(security = @SecurityRequirement(name = "BearerToken"))
    public List<ProductDto> getProductsFromUserBasket() {
        return productMapper.productsToProductsDto(basketService.getProducts());
        //wywolac metode z mappera i przekazac do niej to co zwraca metoda z serwisu


    }

    @DeleteMapping("/{productId}")
    @PreAuthorize("isAuthenticated()")
    @Operation(security = @SecurityRequirement(name = "BearerToken"))
    public void deleteProductFromUserBasket(@PathVariable Long productId){
        basketService.deleteProductFromBasket(productId);
    }

    @DeleteMapping
    @PreAuthorize("isAuthenticated()")
    @Operation(security = @SecurityRequirement(name = "BearerToken"))
    public void deleteProductsFromUserBasket(){
        basketService.clearBasket();
    }

    @PatchMapping("/substract-product")
    @PreAuthorize("isAuthenticated()")
    @Operation(security = @SecurityRequirement(name = "BearerToken"))
    public void oddProductFromUserBasket(@RequestBody @Valid BasketDto basketDto){
        basketService.oddProductFromBasket(basketDto.getProductId(), basketDto.getQuantity());
    }
//mozna przypisac do zmiennej

    //dodac walidacje quantity nie mniejsze niz 1
}
