package com.strzalkom.shop.controller;

import com.strzalkom.shop.mapper.HistoryMapper;
import com.strzalkom.shop.model.dto.OrderDto;
import com.strzalkom.shop.model.dto.ProductDto;
import com.strzalkom.shop.model.dto.UserDto;
import com.strzalkom.shop.repository.OrderRepository;
import com.strzalkom.shop.repository.ProductRepository;
import com.strzalkom.shop.repository.UserRepository;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/history")
@PreAuthorize("hasAuthority('SCOPE_ADMIN')")
public class HistoryController {
    //wstrzykiwanie user repo
    // łatwiej pod względem testów jednostkowych

    private final UserRepository userRepository;
    private final ProductRepository productRepository;
    private final OrderRepository orderRepository;
    private final HistoryMapper historyMapper;



    @GetMapping("/user/{id}")
    @Operation(security = @SecurityRequirement(name = "BearerToken"))
    Page<UserDto> getUserHistory(@PathVariable Long id, @RequestParam int page, @RequestParam int size) {       //revision tabelki audytowe
        return userRepository.findRevisions(id, PageRequest.of(page, size)).map(historyMapper::historyToUserDto);
    }

    @GetMapping("/product/{id}")
    @Operation(security = @SecurityRequirement(name = "BearerToken"))
    Page<ProductDto> getProductHistory(@PathVariable Long id, @RequestParam int page, @RequestParam int size) {
        return productRepository.findRevisions(id, PageRequest.of(page, size)).map(historyMapper::historyToProductDto);
    }

    @GetMapping("/orders/{id}")
    @Operation(security = @SecurityRequirement(name = "BearerToken"))
    Page<OrderDto> getOrderHistory(@PathVariable Long id, @RequestParam int page, @RequestParam int size){
        return orderRepository.findRevisions(id, PageRequest.of(page, size)).map(historyMapper::historyToOrderDto);
    }

    //rozbudowanie mapstuct o nowa funkcjonalnosc
}
