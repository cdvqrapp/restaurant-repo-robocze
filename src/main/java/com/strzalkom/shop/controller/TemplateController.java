package com.strzalkom.shop.controller;

import com.strzalkom.shop.model.dao.Template;
import com.strzalkom.shop.service.TemplateService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/template")
@RequiredArgsConstructor
public class TemplateController {
    private final TemplateService templateService;

    @PostMapping
//    @PreAuthorize("hasAuthority('SCOPE_ADMIN')")
    @ResponseStatus(HttpStatus.CREATED)
    @PreAuthorize("hasAuthority('SCOPE_ADMIN')")
    @Operation(security = @SecurityRequirement(name = "BearerToken"))
    public Template saveTemplate(@RequestBody @Valid Template template) {
        return templateService.save(template);
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasAuthority('SCOPE_ADMIN')")
    @Operation(security = @SecurityRequirement(name = "BearerToken"))
    public void deleteTemplate(@PathVariable Long id) {
        templateService.deleteById(id);
    }

    @GetMapping("/{id}")
    @PreAuthorize("hasAuthority('SCOPE_ADMIN')")
    @Operation(security = @SecurityRequirement(name = "BearerToken"))
    public Template getTemplate(@PathVariable Long id) {
        return templateService.findById(id);
    }

    @PutMapping("/{id}")
    @PreAuthorize("hasAuthority('SCOPE_ADMIN')")
    @Operation(security = @SecurityRequirement(name = "BearerToken"))
    public Template updateTemplate(@RequestBody Template template, @PathVariable Long id){
        return templateService.update(template, id);
    }

}
