package com.strzalkom.shop.controller;

import com.strzalkom.shop.mapper.ProductMapper;
import com.strzalkom.shop.model.dto.ProductDto;
import com.strzalkom.shop.service.ProductService;
import com.strzalkom.shop.validator.ExtensionValid;
import com.strzalkom.shop.validator.group.Create;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;

@Slf4j
@Validated // w przypadku dodawania swojeje adnotacji do parametru metody to trzeba dodac validated dla klasy i metody
@RestController
@RequestMapping("/api/products")
@RequiredArgsConstructor
public class ProductController {

    private final ProductService productService;
    private final ProductMapper productMapper;

    @PreAuthorize("hasAuthority('SCOPE_ADMIN')")
    @Validated(Create.class)
    @PostMapping(consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    @Operation(security = @SecurityRequirement(name = "BearerToken"))
    public ProductDto saveProduct(@RequestPart @Valid ProductDto product, @RequestPart @ExtensionValid MultipartFile image) {
        return productMapper.productToProductDto(productService.save(productMapper.productDtoToProduct(product), image));
    }

    @GetMapping("/{id}")
    public ProductDto getProduct(@PathVariable Long id) {
        return productMapper.productToProductDto(productService.findById(id));
    }

    @PutMapping(value = "/{id}", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    @Validated
    @PreAuthorize("hasAuthority('SCOPE_ADMIN')")
    @Operation(security = @SecurityRequirement(name = "BearerToken"))
    public ProductDto updateProduct(@RequestPart ProductDto product, @PathVariable Long id, @RequestPart @ExtensionValid MultipartFile image) {
        return productMapper.productToProductDto(productService.update(productMapper.productDtoToProduct(product), id , image));
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasAuthority('SCOPE_ADMIN')")
    @Operation(security = @SecurityRequirement(name = "BearerToken"))
    public void deleteProduct(@PathVariable Long id) {
        productService.deleteById(id);
    }

    @GetMapping
    public Page<ProductDto> getProductPage(@RequestParam int page, @RequestParam int size) {
        return productService.getPage(PageRequest.of(page, size)).map(productMapper::productToProductDto);
    }

    @GetMapping("/category/{categoryId}")
    public Page<ProductDto> getProductCategory(@RequestParam int page, @RequestParam int size, @PathVariable Long categoryId){
        return productService.getProductsByCategory(categoryId, PageRequest.of(page, size)).map(productMapper::productToProductDto);
    }
}
