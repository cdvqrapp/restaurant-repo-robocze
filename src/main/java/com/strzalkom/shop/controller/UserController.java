package com.strzalkom.shop.controller;

import com.strzalkom.shop.mapper.UserMapper;
import com.strzalkom.shop.model.dao.User;
import com.strzalkom.shop.model.dto.UserDto;
import com.strzalkom.shop.service.UserService;
import com.strzalkom.shop.service.impl.UserServiceImpl;
import com.strzalkom.shop.validator.group.Create;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.UUID;

@RestController
@RequestMapping("/api/users")//prefix w linku do każdego endpointu
@RequiredArgsConstructor
@Validated // włącza walidację dla metod validated (defaultowo wyłączona)
public class UserController {

    private final UserMapper userMapper;  //wstrzykiwanie zależności  przekazywanie w jednym argumencie konstruktora parametru zmiennej jakiegoś typu
    private final UserService userService;

    //Stworzyć model RepoService dla produkt

    @Validated(Create.class) // włączenie naszej walidacji tylko dla tej metody ( dla grupy create)
    @PostMapping
    @PreAuthorize("hasAuthority('SCOPE_ADMIN') || isAnonymous()")  //admin i niezalogowany user
    @ResponseStatus(HttpStatus.CREATED)
    public UserDto saveUser(@RequestBody @Valid UserDto user) {
        return userMapper.userToUserDto(userService.save(userMapper.userDtoToUser(user))); //userService odpowiada za zapis (usermapper mapuje użytkownika dto na użytkownika)
    }


    //@PathVariable - id podawane po /2  -wart id 2
    //@RequestParam - po ?parameter=wartosc&drugiparametr=wartosc2


    @GetMapping("/{id}")
    @Operation(security = @SecurityRequirement(name = "BearerToken"))
    @PreAuthorize("hasAuthority('SCOPE_ADMIN')")
    public UserDto getUser(@PathVariable Long id)  /* cała linijka jest nagłówkiem funkcji */ {
        User user = userService.findById(id);
        return userMapper.userToUserDto(user);

    }

    @PutMapping("/{id}")
    @Operation(security = @SecurityRequirement(name = "BearerToken"))
    @PreAuthorize("hasAuthority('SCOPE_ADMIN') || @securityService.hasAccessToUser(#id) ") //spell  # odwołanie do parametru do ktorego zostala dodana adnotacja z parametru funkcji update user
    public UserDto updateUser(@RequestBody UserDto user, @PathVariable Long id) {
        return userMapper.userToUserDto(userService.update(userMapper.userDtoToUser(user), id));
    }

    @DeleteMapping("/{id}")
    @Operation(security = @SecurityRequirement(name = "BearerToken"))
    @PreAuthorize("hasAuthority('SCOPE_ADMIN')")
    public void deleteUser(@PathVariable Long id) {
        userService.deleteById(id);
    }

    @GetMapping
    @Operation(security = @SecurityRequirement(name = "BearerToken"))
    @PreAuthorize("hasAuthority('SCOPE_ADMIN')") //tylko admin ma dostęp do metody
    public Page<UserDto> getUserPage(@RequestParam int page, @RequestParam int size) {          //sparametryzowany Page o UserDto
        return userService.getPage(PageRequest.of(page, size)).map(userMapper::userToUserDto); //wywołanie funkcji map na getPage i metodą ref zmapowanie na userDto
    }

    @GetMapping("/current")
    @PreAuthorize("isAuthenticated()")
    public UserDto getCurrentUser(){
        return userMapper.userToUserDto(userService.findCurrentUser());
    }


    @GetMapping("/activate")//get na patch
    @PreAuthorize("isAnonymous()")
    public void activateUser(@RequestParam UUID activatedToken){
        userService.activateUser(activatedToken);

    }

    //JSON
    //number -liczby, string - tekst , boolean - true/false, Object - {}}, Lista/Tablica - []

    //Metody HTTP
    //PUT -- podmienia cały obiekt PATCH- podmienia część obiektu, GET(pobieranie), POST(zapis), DELETE(usuwanie obiektu)


    // 200 - OK , 201 - created, 202 - accepted, 500 - internal server error, 404 - not found, 401 unatuhorized, 403 - acces denided
    //409 - conflict - na podłożu bazy danych ten sam obiekt w db
}


//przerobić DTO

