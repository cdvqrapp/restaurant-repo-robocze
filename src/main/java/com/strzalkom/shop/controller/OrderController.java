package com.strzalkom.shop.controller;

import com.strzalkom.shop.mapper.OrderDetailsMapper;
import com.strzalkom.shop.mapper.OrderMapper;
import com.strzalkom.shop.mapper.ProductMapper;
import com.strzalkom.shop.model.OrderStatus;
import com.strzalkom.shop.model.dao.Order;
import com.strzalkom.shop.model.dao.OrderDetails;
import com.strzalkom.shop.model.dao.User;
import com.strzalkom.shop.model.dto.OrderDetailsDto;
import com.strzalkom.shop.model.dto.ProductDto;
import com.strzalkom.shop.repository.OrderRepository;
import com.strzalkom.shop.service.OrderDetailsService;
import com.strzalkom.shop.service.OrderService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/orders")
@RequiredArgsConstructor
public class OrderController {

    private final OrderDetailsService orderDetailsService;
    private final ProductMapper productMapper;
    private final OrderService orderService;

    private final OrderDetailsMapper orderDetailsMapper;


    @PostMapping
    @PreAuthorize("isAuthenticated()")
    @Operation(security = @SecurityRequirement(name = "BearerToken"))
    public void saveOrder(@RequestBody OrderDetailsDto orderDetailsDto) {
        orderDetailsService.createOrder(orderDetailsDto.getClientDescription(), orderDetailsDto.getTableNumber());
    }

    @PatchMapping
    @PreAuthorize("hasAuthority('SCOPE_ADMIN') || @securityService.hasAccessToOrderDetails(#orderDetailsDto.id)")
    @Operation(security = @SecurityRequirement(name = "BearerToken"))
    public void changeTableObject(@RequestBody OrderDetailsDto orderDetailsDto){
        orderDetailsService.changeTableIdInOrder(orderDetailsDto.getTableNumber(), orderDetailsDto.getId());
    }

    @GetMapping("/all/{id}")
    @PreAuthorize("hasAuthority('SCOPE_ADMIN')")
    @Operation(security = @SecurityRequirement(name = "BearerToken"))
    public List<Order> getCurrentOrderStatus(@PathVariable Long id){
        return orderService.getByOrderDetailsId(id);
    }

    @GetMapping("/current/{id}")
    @PreAuthorize("hasAuthority('SCOPE_ADMIN') || @securityService.hasAccessToOrderDetails(#id)")
    @Operation(security = @SecurityRequirement(name = "BearerToken"))
    public OrderDetails getCurrentOrder(@PathVariable Long id){
        return orderDetailsService.getById(id);
    }

    @GetMapping("/products/{id}")
    @PreAuthorize("hasAuthority('SCOPE_ADMIN') || @securityService.hasAccessToOrderDetails(#id)")
    @Operation(security = @SecurityRequirement(name = "BearerToken"))
    public List<ProductDto> getProductsFromOrder(@PathVariable Long id){
        return productMapper.productsToProductsDto(orderService.getProductListByUser(id));
    }
    @GetMapping("/details/{id}")
    @PreAuthorize("hasAuthority('SCOPE_ADMIN')|| @securityService.hasAccessToOrderDetails(#id)")
    @Operation(security = @SecurityRequirement(name = "BearerToken"))
    public List<ProductDto> getProductsFromOrderDetails(@PathVariable Long id){
        return productMapper.productsToProductsDto(orderService.getProductListByOrder(id));
    }

    @GetMapping("get-all")
    @PreAuthorize("hasAuthority('SCOPE_ADMIN')")
    @Operation(security = @SecurityRequirement(name = "BearerToken"))
    public Page<OrderDetails> getAllOrderDetails(@RequestParam int page, @RequestParam int size) {     //page = 0 size 100 pierwsze 100 sie wyswietli      //sparametryzowany Page o UserDto
        return orderDetailsService.getAll(PageRequest.of(page, size)); //wywołanie funkcji map na getPage i metodą ref zmapowanie na userDto
    }



    @PutMapping("/ready-to-pay")
    @PreAuthorize("hasAuthority('SCOPE_ADMIN') || @securityService.hasAccessToOrderDetails(#orderDetailsDto.id)")
    @Operation(security = @SecurityRequirement(name = "BearerToken"))
    public void orderReadyToPay(@RequestBody OrderDetailsDto orderDetailsDto){
        orderDetailsService.changeOrderStatus(orderDetailsDto.getId(), OrderStatus.WAITINGFORPAYMENT);
    }

    @GetMapping
    @Operation(security = @SecurityRequirement(name = "BearerToken"))
    @PreAuthorize("isAuthenticated()") //tylko admin ma dostęp do metody
    public Page<OrderDetails> getOrderPageForCurrentUser(@RequestParam int page, @RequestParam int size) {     //page = 0 size 100 pierwsze 100 sie wyswietli      //sparametryzowany Page o UserDto
        return orderDetailsService.getPage(PageRequest.of(page, size)); //wywołanie funkcji map na getPage i metodą ref zmapowanie na userDto
    }


    @PatchMapping("/status")
    @PreAuthorize("hasAuthority('SCOPE_ADMIN') || @securityService.hasAccessToOrderDetails(#orderDetailsDto.id)")
    @Operation(security = @SecurityRequirement(name = "BearerToken"))
    public void changeOrderStatus(@RequestBody OrderDetailsDto orderDetailsDto){
        orderDetailsService.changeOrderStatus(orderDetailsDto.getId(), orderDetailsDto.getOrderStatus());
    }

    @PatchMapping("/payment")
    @PreAuthorize("hasAuthority('SCOPE_ADMIN') || @securityService.hasAccessToOrderDetails(#orderDetailsDto.id)")
    @Operation(security = @SecurityRequirement(name = "BearerToken"))
    public void changeOrderPaymentToCard(@RequestBody OrderDetailsDto orderDetailsDto){
        orderDetailsService.changeOrderPaymentMethod(orderDetailsDto.getId(), orderDetailsDto.getPaymentMethod());
    }


//    @PostMapping("/table")
//    @PreAuthorize("isAuthenticated()")
//    @Operation(security = @SecurityRequirement(name = "BearerToken"))
//    public void saveOrderWithTable(@RequestBody OrderDetailsDto orderDetailsDto) {
//        orderDetailsService.createOrderWithTable(orderDetailsDto.getDescription(), orderDetailsDto.getTableObjectId(),
//                productMapper.productsDtoToProducts(orderDetailsDto.getProductDtoList()));
//    }




}
