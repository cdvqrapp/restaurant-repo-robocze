package com.strzalkom.shop.helper;

import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

@Component
public class FileHelper {

    public void deleteFile(String path) throws IOException {
        Files.delete(Paths.get(path));
    }

    public void saveFile(InputStream inputStream, String path) throws IOException{
        Files.copy(inputStream, Paths.get(path), StandardCopyOption.REPLACE_EXISTING);
    }
}
