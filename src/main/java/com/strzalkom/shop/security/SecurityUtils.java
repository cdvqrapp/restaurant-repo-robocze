package com.strzalkom.shop.security;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

@NoArgsConstructor(access = AccessLevel.PRIVATE) // bezargumentowy konstruktor prywatny
public class SecurityUtils {            //klasa pomocnicza dla metod utils - constraints klasa pomocnicza dla zmiennych
    //wszystkie metody sa statyczne w utils
    //wszystkie zmienne sa statyczne w constraints
    //konstruktory bezargumentowe ZAWSZE musza byc prywatne, nie moze byc konstruktorow wieloargumentowych

    //nic nie przyjmuje, zwraca stringa
  public static String getCurrentUserEmail(){       // modyfikator dostepu public poniewaz w innych pakietach tez korzystamy
      Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

      return authentication != null ? authentication.getName() : null ;   //Elvis expression
    }


}
