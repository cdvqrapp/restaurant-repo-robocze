package com.strzalkom.shop.security;

import org.springframework.data.domain.AuditorAware;
import org.springframework.stereotype.Component;

import java.util.Optional;

import static com.strzalkom.shop.security.SecurityUtils.getCurrentUserEmail;

@Component  // jezeli zadna andotacja nie pasuje to okreslamy bean jako component
public class AuditorAwareImpl implements AuditorAware<String> {


    @Override
    public Optional<String> getCurrentAuditor() {
        return Optional.ofNullable(getCurrentUserEmail());
//jezeli null to pusty optional

    }
}
