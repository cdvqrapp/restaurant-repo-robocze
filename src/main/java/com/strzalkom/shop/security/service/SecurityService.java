package com.strzalkom.shop.security.service;


import com.strzalkom.shop.service.OrderDetailsService;
import com.strzalkom.shop.service.OrderService;
import com.strzalkom.shop.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

//@Bean //rejestruje klase jako bean w springu TYLKO dla metody
@Service
@RequiredArgsConstructor
public class SecurityService {

    private final UserService userService;
    private final OrderDetailsService orderDetailsService;
    private final OrderService orderService;



    //sprawdzic czy id aktualnego usera zalogowanego jest rowne id usera z parametru funkcji
    public boolean hasAccessToOrderDetails(Long id){
        return userService.findCurrentUser().getId().equals(orderDetailsService.getById(id).getUser().getId());
    }
    public boolean hasAccessToOrder(Long id){
        return userService.findCurrentUser().getId().equals(orderService.getById(id).getOrderDetails().getUser().getId());
    }


   public boolean hasAccessToUser(Long id) {

        return userService.findCurrentUser().getId().equals(id);
    }



}
