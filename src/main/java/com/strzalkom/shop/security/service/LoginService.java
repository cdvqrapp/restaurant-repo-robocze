package com.strzalkom.shop.security.service;

import com.nimbusds.jwt.JWTClaimsSet;
import com.strzalkom.shop.model.dto.TokenDto;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.oauth2.jwt.JwtClaimsSet;
import org.springframework.security.oauth2.jwt.JwtEncoder;
import org.springframework.security.oauth2.jwt.JwtEncoderParameters;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class LoginService {

    private final AuthenticationManager authenticationManager;
    private final JwtEncoder jwtEncoder;

    public TokenDto login(String email, String password) {
        UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(email, password);
        Authentication authentication = authenticationManager.authenticate(authenticationToken);
        //sprawdzic flow do logowania usera - springsecurity

        JwtClaimsSet claims = JwtClaimsSet.builder()  //dekorator czy adapter?
                .subject(authentication.getName())
                .expiresAt(Instant.now().plus(1, ChronoUnit.DAYS))
                .claim("scope", authentication.getAuthorities().stream()
                        .map(GrantedAuthority::getAuthority)   //metoda referencyjna bo uzywam jednej metody
                        .collect(Collectors.joining(" "))) //laczenie nstringow ze strumienia po spacji
                .build();

        String tokenValue = jwtEncoder.encode(JwtEncoderParameters.from(claims)).getTokenValue();

        TokenDto tmp = new TokenDto();
        tmp.setToken(tokenValue);


        return tmp;
    }
}
