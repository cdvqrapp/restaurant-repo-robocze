package com.strzalkom.shop.security;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
@Slf4j
@Deprecated
public class JwtAuthorizationFilter extends BasicAuthenticationFilter {


    public JwtAuthorizationFilter(AuthenticationManager authenticationManager) {
        super(authenticationManager);
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws IOException, ServletException {
        String token = request.getHeader(HttpHeaders.AUTHORIZATION);
        if (token == null || !token.startsWith("Bearer ")) {
            chain.doFilter(request, response);
            return; //przerywa działanie funkcji
        }
        try {
            Claims claims = Jwts.parser()
                    .setSigningKey("Test12345")
                    .parseClaimsJws(token.substring(7))
                    .getBody();


            String email = claims.getSubject();
            String authorieties = claims.get("authorieties", String.class);//klucz pierwszy argument określenie typu zwracanego

            List<GrantedAuthority> grantedAuthorities = new LinkedList<>();
            if (authorieties != null && !authorieties.isEmpty()) {
                grantedAuthorities = Arrays.stream(authorieties.split(","))
                        .map(SimpleGrantedAuthority::new)
                        .collect(Collectors.toList());


            }

            UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken =
                    new UsernamePasswordAuthenticationToken(email, null, grantedAuthorities);

            SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken); //wstawienie usera do kontekstu security
            //aplikacja wie jaki uzytkownik wykonuje w tym momencie dany request
            //chain of responsibility
            chain.doFilter(request, response);

        } catch (ExpiredJwtException e) {

            log.warn("Token has expired", e);
            response.setStatus(401);
        }

    }


}
