package com.strzalkom.shop.security.controller;

import com.strzalkom.shop.model.dto.LoginDto;
import com.strzalkom.shop.model.dto.TokenDto;
import com.strzalkom.shop.security.service.LoginService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/login")//prefix w linku do każdego endpointu
@RequiredArgsConstructor
public class LoginController {

    private final LoginService loginService;

    @PostMapping
    public TokenDto login(@RequestBody LoginDto loginDto){
        return loginService.login(loginDto.getEmail(), loginDto.getPassword());
    }
}
