package com.strzalkom.shop.security;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.strzalkom.shop.model.dto.LoginDto;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.impl.DefaultClaims;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collections;
import java.util.Date;
import java.util.stream.Collectors;

@Slf4j
@Deprecated
public class JwtAuthenticationFilter extends UsernamePasswordAuthenticationFilter {
    //object mapper - mapowanie obiektów javowych na json i odwrotnie, główny mapper wykorzystywany do stringa  ]

    private final ObjectMapper objectMapper;


    public JwtAuthenticationFilter(AuthenticationManager authenticationManager, ObjectMapper objectMapper) {
        super(authenticationManager); //wywołanie konstruktora klasy nadrzędnej (klasa po której dziedziczymy) przez to brak adnotacji
        this.objectMapper = objectMapper; //przypisanie zmiennej lokalnej do globalnej
        setFilterProcessesUrl("/api/login");
    }

    @Override //obsługa requestu logującego
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException {
        if (!request.getMethod().equals(HttpMethod.POST.name())) {
            throw new AuthenticationServiceException("http method from request " + request.getMethod() + " is not valid");
        }

        try {
            LoginDto loginDto = objectMapper.readValue(request.getInputStream(), LoginDto.class);//wyjątek pobranie ciała requestu, drugi paramert jako logindto
            UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(loginDto.getEmail(), loginDto.getPassword());
            return getAuthenticationManager().authenticate(usernamePasswordAuthenticationToken);
        } catch (Exception e) {
            log.error("Error: ", e);
            throw new AuthenticationServiceException("http method from request " + request.getMethod() + " is not valid");

        }
    }


    @Override
    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain, Authentication authResult) throws IOException, ServletException {
        Claims claims = new DefaultClaims() //zmienna pod którą można wstawiać wartości dzięki którym będzie wygenerowany token
                .setSubject(authResult.getName())
                .setExpiration(new Date(System.currentTimeMillis() + 1000 * 60 * 60 * 24)); //aktualny czas na systemie  * cały dzień w milisekundach
        claims.put("authorieties", authResult.getAuthorities().stream()
                .map(GrantedAuthority::getAuthority)  //zmiana wartości
                .collect(Collectors.joining(","))); //połączenie i rozdzielenie


        String token = Jwts.builder()
                .setClaims(claims)
                .signWith(SignatureAlgorithm.HS512, "Test12345") //tworzenie tokenu , key zazwyczaj jest generowany
                .compact();//stworzenie tokenu

        response.setContentType(MediaType.APPLICATION_JSON_VALUE); //w response zwracamy jsona
        objectMapper.writeValue(response.getWriter(), Collections.singletonMap("token", token)); //jednoelementowa mapa przekazywana jako json do odpowiedzi

    }

}


//UsernamePasswordAuthenticationFilter  - poczytać

//flyweight - wzorzec projektowy
