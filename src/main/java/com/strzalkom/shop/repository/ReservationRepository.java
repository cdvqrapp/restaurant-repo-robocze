package com.strzalkom.shop.repository;

import com.strzalkom.shop.model.dao.Reservation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.history.RevisionRepository;

import java.util.List;
import java.util.Optional;

public interface ReservationRepository extends JpaRepository<Reservation, Long>, RevisionRepository<Reservation, Long, Integer> {

    List<Reservation>findByTableObjectId(Long tableId);
//    Optional<Reservation>findByTableId(Long tableId);

}
