package com.strzalkom.shop.repository;

import com.strzalkom.shop.model.dao.Basket;



import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;


public interface BasketRepository extends JpaRepository<Basket, Long> {

    Optional<Basket> findByUserIdAndProductId(Long userId, Long productId);

    void deleteByUserIdAndProductId(Long userId, Long productId);                  //basket -> User -> User.id autogenerowanie zapytania sql

    void deleteByUserId(Long userId);

    List<Basket> findByUserId(Long userId);

}
