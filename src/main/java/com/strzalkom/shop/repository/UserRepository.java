package com.strzalkom.shop.repository;

import com.strzalkom.shop.model.dao.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.history.RevisionRepository;

import java.util.Optional;
import java.util.UUID;

public interface UserRepository extends JpaRepository<User, Long>, RevisionRepository<User, Long, Integer> { // klasa reprezentujaca tabelke i typ id

    //komunikacja z warstwą danych, robi wszystkie zapytania
    Optional<User> findByEmailAndActivatedTokenIsNull(String email);
    Optional<User> findByEmail(String email);

    Optional<User> findByActivatedToken(UUID activatedToken);


}
