package com.strzalkom.shop.repository;

import com.strzalkom.shop.model.dao.Product;
import com.strzalkom.shop.model.dao.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.history.RevisionRepository;

import java.util.List;
import java.util.Optional;

public interface ProductRepository extends JpaRepository<Product, Long>, RevisionRepository<Product, Long, Integer> {

    //warstwa do komunikacji z wartstwą danych
//zapyutanie do bazy danych

    //grouping i having nie zadzialaja w jpa

    Page<Product> findByCategoryId(Long categoryId, Pageable pageable);

    List<Product> findByIdIn(List<Long> ids);
}
