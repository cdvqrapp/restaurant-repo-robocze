package com.strzalkom.shop.repository;

import com.strzalkom.shop.model.dao.TableObject;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.history.RevisionRepository;

import java.util.Optional;

public interface TableRepository extends JpaRepository<TableObject, Long>, RevisionRepository<TableObject, Long, Integer> { // klasa reprezentujaca tabelke i typ id

    Optional<TableObject> findByTableNumber(Long tableNumber);
    //komunikacja z warstwą danych, robi wszystkie zapytania
//    Optional<Table> findByOrder(Order order);
}
