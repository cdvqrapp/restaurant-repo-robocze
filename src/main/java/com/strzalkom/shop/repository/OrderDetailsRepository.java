package com.strzalkom.shop.repository;


import com.strzalkom.shop.model.dao.Order;
import com.strzalkom.shop.model.dao.OrderDetails;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.history.RevisionRepository;

import java.util.List;

public interface OrderDetailsRepository extends JpaRepository<OrderDetails, Long>, RevisionRepository<OrderDetails, Long, Integer> {

    Page<OrderDetails> findByUserId(Long userId, Pageable pageable);



}
