package com.strzalkom.shop.repository;

import com.strzalkom.shop.model.dao.Order;
import com.strzalkom.shop.model.dao.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.history.RevisionRepository;

import java.util.List;

public interface OrderRepository extends JpaRepository<Order, Long>, RevisionRepository<Order, Long, Integer> {

    List<Order>findByOrderDetailsId(Long orderDetailsId);
    List<Order>findByOrderDetailsUserId(Long userId);

//    Long findByUser(String email);
}
