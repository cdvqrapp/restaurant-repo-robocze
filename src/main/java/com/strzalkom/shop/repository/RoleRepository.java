package com.strzalkom.shop.repository;

import com.strzalkom.shop.model.dao.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.history.RevisionRepository;

import java.util.Optional;

public interface RoleRepository extends JpaRepository<Role, Long>, RevisionRepository<Role, Long, Integer> {
    //customowa metoda do zapytań do tabelki role (db)

    Optional<Role> findByName(String name);
    //wszystkie metody defaultowe są abstrakcyjne w INTERFEJSIE - nie mają ŻADNYCH CIAŁ !!!!

}
