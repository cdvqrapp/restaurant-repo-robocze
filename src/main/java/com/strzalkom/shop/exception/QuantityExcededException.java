package com.strzalkom.shop.exception;

public class QuantityExcededException extends RuntimeException{
    public QuantityExcededException(String message) {
        super(message);
    }


}
