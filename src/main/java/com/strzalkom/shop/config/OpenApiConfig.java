package com.strzalkom.shop.config;

import io.swagger.v3.oas.models.Components;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.security.SecurityScheme;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class OpenApiConfig {

    @Bean
    public OpenAPI openAPI(@Value("${version}") String version) {
        return new OpenAPI()
                .info(new Info()
                        .title("Restaurant API")
                        .version(version)
                        .description("This is Restaurant API")
                        .contact(new Contact()
                                .email("mstrzalko@edu.cdv.pl")
                                .name("Mateusz Strzałko")
                                .url("githublink")))
                .components(new Components()
                        .addSecuritySchemes("BearerToken", new SecurityScheme()
                                .type(SecurityScheme.Type.HTTP)
                                .scheme("bearer")
                                .bearerFormat("JWT")));
    }
}
//podawanie tokenu w swagger kłodka -> Bearer {wartość tokenu}