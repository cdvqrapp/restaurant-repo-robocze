package com.strzalkom.shop.mapper;

import com.strzalkom.shop.model.dao.Product;
import com.strzalkom.shop.model.dto.ProductDto;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface ProductMapper {

    ProductDto productToProductDto(Product product);

    Product productDtoToProduct(ProductDto productDto);

    List<ProductDto> productsToProductsDto(List<Product> products);

    List<Product> productsDtoToProducts(List<ProductDto> productsDto);
}
