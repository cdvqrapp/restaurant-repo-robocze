package com.strzalkom.shop.mapper;

import com.strzalkom.shop.model.dao.TableObject;
import com.strzalkom.shop.model.dto.TableDto;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface TableMapper {
    TableDto tableToTableDto(TableObject tableObject);

    TableObject tableDtoToTable(TableDto tableDto);

}
