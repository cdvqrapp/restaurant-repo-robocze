package com.strzalkom.shop.mapper;

import com.strzalkom.shop.model.dao.Role;
import com.strzalkom.shop.model.dao.User;
import com.strzalkom.shop.model.dto.UserDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

import java.util.List;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
//implementacja wygeneruje się z adnotacją component dzięki temu że w stringu jest "spring"
public interface UserMapper {
//default - metoda ktora posiada własne ciało

    @Mapping(target = "password", ignore = true)
    @Mapping(source = "roles", target = "roles", qualifiedByName = "mapRoles")
    UserDto userToUserDto(User user);

    @Mapping(target = "roles", ignore = true)
    User userDtoToUser(UserDto userDto);

    @Named("mapRoles")
    default List<String> mapRoles(List<Role> roles) {
        if (roles != null) {
            return roles.stream()
                    .map(Role::getName)
                    .collect(Collectors.toList());
        }
        return null;
    }
}


