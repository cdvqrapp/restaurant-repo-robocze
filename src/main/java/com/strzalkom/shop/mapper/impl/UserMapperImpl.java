//package com.strzalkom.shop.mapper.impl;
//
//import com.strzalkom.shop.mapper.UserMapper;
//import com.strzalkom.shop.model.dao.User;
//import com.strzalkom.shop.model.dto.UserDto;
//import org.springframework.stereotype.Component;
//
//@Component
//public class UserMapperImpl implements UserMapper {
//
//    @Override
//    public UserDto userToUserDto(User user) { // od aplikacji do klienta
//        return UserDto.builder()                //wzorzec projektowy buildera
//                .id(user.getId())
//                .firstName(user.getFirstName())
//                .lastName(user.getLastName())
//                .email(user.getEmail())
//                .build();
//    }
//
//    @Override
//    public User userDtoToUser(UserDto userDto) {   // od klienta do aplikacji (zapis do DB)
//        return User.builder()
//                .id(userDto.getId())
//                .firstName(userDto.getFirstName())
//                .lastName(userDto.getLastName())
//                .email(userDto.getEmail())
//                .password(userDto.getPassword())
//                .build();
//    }
//}
