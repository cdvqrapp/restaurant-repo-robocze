//package com.strzalkom.shop.mapper.impl;
//
//import com.strzalkom.shop.mapper.ProductMapper;
//import com.strzalkom.shop.model.dao.Product;
//import com.strzalkom.shop.model.dto.ProductDto;
//import org.springframework.stereotype.Component;
//
//@Component
//public class ProductMapperImpl implements ProductMapper {
//
//    @Override
//    public ProductDto productToProductDto(Product product) { // od aplikacji do klienta
//        return ProductDto.builder()                //wzorzec projektowy buildera
//                .id(product.getId())
//                .name(product.getName())
//                .quantity(product.getQuantity())
//                .description(product.getDescription())
//                .price(product.getPrice())
//                .code(product.getCode())
//                .build();
//    }
//
//    @Override
//    public Product productDtoToProduct(ProductDto productDto) {   // od klienta do aplikacji (zapis do DB)
//        return Product.builder()
//                .id(productDto.getId())
//                .name(productDto.getName())
//                .quantity(productDto.getQuantity())
//                .description(productDto.getDescription())
//                .price(productDto.getPrice())
//                .code(productDto.getCode())
//                .build();
//    }
//}