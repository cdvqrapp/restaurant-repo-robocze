package com.strzalkom.shop.mapper;

import com.strzalkom.shop.model.dao.Order;
import com.strzalkom.shop.model.dto.OrderDto;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface OrderMapper {
    OrderDto orderToOrderDto(Order order);

    Order orderDtoToOrder(OrderDto orderDto);

    List<OrderDto> ordersToOrdersDto(List<Order> orders);
}
