package com.strzalkom.shop.mapper;

import com.strzalkom.shop.model.dao.OrderDetails;
import com.strzalkom.shop.model.dto.OrderDetailsDto;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface OrderDetailsMapper {

    OrderDetails orderDetailsDtoToOrderDetails(OrderDetailsDto orderDetailsDto);

    OrderDetailsDto orderDetailsToOrderDetailsDto(OrderDetails orderDetails);
}
