package com.strzalkom.shop.mapper;

import com.strzalkom.shop.model.dao.Order;
import com.strzalkom.shop.model.dao.Product;
import com.strzalkom.shop.model.dao.User;
import com.strzalkom.shop.model.dto.OrderDto;
import com.strzalkom.shop.model.dto.ProductDto;
import com.strzalkom.shop.model.dto.UserDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.data.history.Revision;

@Mapper(componentModel = "spring")   //generuje implementacje do metod w interfejsie    //maven clean -> compile
public interface HistoryMapper {

    @Mapping(source = "entity.id", target = "id")
    @Mapping(source = "entity.firstName", target = "firstName")
    @Mapping(source = "entity.lastName", target = "lastName")
    @Mapping(source = "entity.email", target = "email")
    @Mapping(source = "requiredRevisionNumber", target = "revisionNumber")

    UserDto historyToUserDto(Revision<Integer, User> revision);  //revision - reprezentuje dane z tabelki audytowej


    @Mapping(source = "entity.id", target ="id")
    @Mapping(source = "entity.name", target ="name")
    @Mapping(source = "entity.quantity", target ="quantity")
    @Mapping(source = "entity.description", target ="description")
    @Mapping(source = "entity.price", target ="price")
    @Mapping(source = "entity.code", target ="code")
    @Mapping(source = "requiredRevisionNumber", target = "revisionNumber")
    ProductDto historyToProductDto(Revision<Integer, Product> revision);



    @Mapping(source = "entity.quantity", target = "quantity")
    @Mapping(source = "requiredRevisionNumber", target = "revisionNumber")
    OrderDto historyToOrderDto(Revision<Integer, Order> revision);


    //dodać inne pola, generować mappera



}
