package com.strzalkom.shop.validator.impl;

import com.strzalkom.shop.config.properties.FileConfig;
import com.strzalkom.shop.validator.ExtensionValid;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.apache.commons.io.FilenameUtils;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

@Component
@RequiredArgsConstructor
public class ExtensionValidImpl implements ConstraintValidator<ExtensionValid, MultipartFile> {
    private final FileConfig fileConfig;


    @Override
    public boolean isValid(MultipartFile value, ConstraintValidatorContext context) {
        String extension = FilenameUtils.getExtension(value.getOriginalFilename());
        context.buildConstraintViolationWithTemplate("Use this extensions: " + fileConfig.getExtensions()).addConstraintViolation();
        return fileConfig.getExtensions().contains(extension);
    }
}

