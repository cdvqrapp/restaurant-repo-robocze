package com.strzalkom.shop.validator;

import com.strzalkom.shop.validator.impl.ExtensionValidImpl;
import com.strzalkom.shop.validator.impl.PasswordValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented // - oznaczenie jako adnotacja
@Target(ElementType.PARAMETER)  //w którym miejscu będziemy mogli używać tej adnotacji (TYPE czyli dla klasy) FIELD dla pola METHOD dla metody, PARAMETER
@Retention(RetentionPolicy.RUNTIME) //validator działa w trakcie działania programu
@Constraint(validatedBy = ExtensionValidImpl.class)  //podawanie klasy
public @interface ExtensionValid {
    String message() default "Wrong extension ";
    Class<?>[] groups() default {};  //włączanie i wyłączanie validatora w odpowiednich miejscach

    Class<? extends Payload>[] payload() default {};   //metoda defaultowa ? - cokolwiek co rozszerza klase payload

}
