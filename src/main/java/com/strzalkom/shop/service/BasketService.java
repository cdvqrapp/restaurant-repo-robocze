package com.strzalkom.shop.service;

import com.strzalkom.shop.model.dao.Basket;
import com.strzalkom.shop.model.dao.Product;

import java.util.List;

public interface BasketService {

    void deleteProductFromBasket(Long productId);

    void updateBasket(Long productId, Integer quantity);

    void clearBasket();

    List<Product> getProducts();

    void oddProductFromBasket(Long productId, Integer quantity);
    List<Basket> getUserBasket(Long userId);
}
