package com.strzalkom.shop.service;

import com.strzalkom.shop.model.OrderStatus;
import com.strzalkom.shop.model.PaymentMethod;
import com.strzalkom.shop.model.dao.OrderDetails;
import com.strzalkom.shop.model.dao.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface OrderDetailsService {

    void changeOrderStatus(Long orderId, OrderStatus orderStatus);

    void createOrder(String description, Long tableId);

    void deleteOrder(OrderDetails order); //dorobic enuma

    void changeOrderPaymentMethod(Long orderId, PaymentMethod paymentMethod);

    Page<OrderDetails> getPage(Pageable pageable);
     void createOrderWithTable(String description, Long tableId, List<Product> productList);

     void changeTableIdInOrder(Long tableNumber, Long orderId);

    OrderDetails getById(Long id);

    Page<OrderDetails> getAll(Pageable pageable);
}
