package com.strzalkom.shop.service;

import com.strzalkom.shop.model.dao.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

public interface ProductService {

    Product save(Product product, MultipartFile image);

    Product update(Product product, Long id, MultipartFile image);

    Product findById(Long id);

    List<Product> findByIds(List<Long> ids);

    void deleteById(Long id);

    Page<Product> getProductsByCategory(Long categoryId,Pageable pageable);

    Page<Product> getPage(Pageable pageable);
}
