package com.strzalkom.shop.service;

import com.strzalkom.shop.model.dao.TableObject;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface TableService {
    TableObject save(TableObject tableObject); //metoda abstrakcyjna nie posiada implementacji

    TableObject update(TableObject tableObject, Long id);

    TableObject findById(Long id);

    void deleteById(Long id);

    TableObject findCurrentTableByOrder();
     TableObject findByNumber(Long tableNumber);

    Page<TableObject> getPage(Pageable pageable);
}
