package com.strzalkom.shop.service.impl;

import com.strzalkom.shop.model.dao.Reservation;
import com.strzalkom.shop.repository.ReservationRepository;
import com.strzalkom.shop.service.ReservationService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor

public class ReservationServiceImpl implements ReservationService {

    private final ReservationRepository reservationRepository;

    @Override
    public Reservation save(Reservation reservation) {
        return null;
    }

    @Override
    public Reservation update(Long id, Reservation reservation) {
        Reservation reservationDb = findById(id);
        reservationDb.setDescription(reservation.getDescription());
        reservationDb.setTableObject(reservation.getTableObject());
        reservationDb.setUser(reservation.getUser());
        reservationDb.setReservationFrom(reservation.getReservationFrom());
        reservationDb.setReservationTo(reservation.getReservationTo());
        return reservationDb;
    }

    @Override
    public void deleteById(Long id) {
        reservationRepository.deleteById(id);
    }

    @Override
    public Reservation findById(Long id) {
        return reservationRepository.getById(id);
    }

    //get reservation Table
    //funkcja zwraca liste rezerwacji dla danego stolu

    @Override
    public List<Reservation> findByTable(Long tableId) {
        return reservationRepository.findByTableObjectId(tableId);
    }



    @Override
    public Page<Reservation> getPage(Pageable pageable) {
        return null;
    }
}
