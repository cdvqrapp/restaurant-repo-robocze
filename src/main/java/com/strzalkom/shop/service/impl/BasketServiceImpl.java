package com.strzalkom.shop.service.impl;

import com.strzalkom.shop.exception.QuantityExcededException;

import com.strzalkom.shop.model.dao.Basket;
import com.strzalkom.shop.model.dao.Product;
import com.strzalkom.shop.model.dao.User;
import com.strzalkom.shop.repository.BasketRepository;
import com.strzalkom.shop.service.BasketService;
import com.strzalkom.shop.service.ProductService;
import com.strzalkom.shop.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class BasketServiceImpl implements BasketService {

    private final BasketRepository basketRepository;
    private final ProductService productService;
    private final UserService userService;


    @Override
    @Transactional
    public void deleteProductFromBasket(Long productId) {
        User userDb = userService.findCurrentUser();
        basketRepository.deleteByUserIdAndProductId(userDb.getId(), productId);

    }



    @Override
    public void updateBasket(Long productId, Integer quantity) {

        User userDb = userService.findCurrentUser();
        basketRepository.findByUserIdAndProductId(userDb.getId(), productId).ifPresentOrElse(basket -> {
            if (basket.getProduct().getQuantity() < basket.getQuantity() + quantity) {
                throw new QuantityExcededException("Wrong quantity for product " + basket.getProduct().getName() + " During updating basket");
            }
            basket.setQuantity(basket.getQuantity() + quantity);
            if(basket.getQuantity() != 0)
            basketRepository.save(basket);
            else
                basketRepository.deleteByUserIdAndProductId(userDb.getId(), productId);


        }, () -> {
            Product product = productService.findById(productId);
            if (quantity > product.getQuantity()) {
                throw new QuantityExcededException("Wrong quantity for product " + product.getName() + " During Added product to basket");

            }
            basketRepository.save(new Basket(null, userDb, product, quantity));


            //dokonczyc metode

        }); // interfejs funkcyjny runnable

        //pierwszy - istnieje // nie istnieje


    }

    @Override
    @Transactional
    public void oddProductFromBasket(Long productId, Integer quantity){
        User userDb = userService.findCurrentUser();
        basketRepository.findByUserIdAndProductId(userDb.getId(), productId).ifPresentOrElse(basket -> {
            if (basket.getProduct().getQuantity() < quantity) {
                throw new QuantityExcededException("Wrong quantity for product " + basket.getProduct().getName() + " During updating basket");
            }
            basket.setQuantity(basket.getQuantity() - quantity);
            if(basket.getQuantity() != 0)
                basketRepository.save(basket);
            else
                basketRepository.deleteByUserIdAndProductId(userDb.getId(), productId);

        }, () -> {
            Product product = productService.findById(productId);
            if (quantity > product.getQuantity()) {
                throw new QuantityExcededException("Wrong quantity for product " + product.getName() + " During Added product to basket");

            }
            basketRepository.save(new Basket(null, userDb, product, quantity));


            //dokonczyc metode

        }); // interfejs funkcyjny runn
    }

    @Override
    @Transactional
    public void clearBasket() {
        User userDb = userService.findCurrentUser();
        basketRepository.deleteByUserId(userDb.getId());

    }

    @Override
    public List<Product> getProducts() {
        User userDb = userService.findCurrentUser();


        return basketRepository.findByUserId(userDb.getId()).stream()
                .map(basket -> {
                    Product basketProduct = basket.getProduct();
                    basketProduct.setQuantity(basket.getQuantity());
                    return basketProduct;
                }) //jak zrobic zeby napisac wiecej niz jedna linijke w lambdzie
                .collect(Collectors.toList());
    }

    @Override
    public List<Basket> getUserBasket(Long userId) {
        return basketRepository.findByUserId(userId);
    }
}
