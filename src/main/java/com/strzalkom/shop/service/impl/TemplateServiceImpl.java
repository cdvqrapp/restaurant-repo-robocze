package com.strzalkom.shop.service.impl;


import com.strzalkom.shop.model.dao.Template;
import com.strzalkom.shop.repository.TemplateRepository;
import com.strzalkom.shop.service.TemplateService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;

@Service
@RequiredArgsConstructor
public class TemplateServiceImpl implements TemplateService {

    private final TemplateRepository templateRepository;

    @Override
    public Template findByName(String name) {

        return templateRepository.findByName(name)
                .orElseThrow(() -> new EntityNotFoundException("Template name: " + name));

    }

    @Override
    public Template findById(Long id) {
        return templateRepository.getById(id);
    }

    @Override
    public Template save(Template template) {
        return templateRepository.save(template);
    }

    @Override
    @Transactional
    public Template update(Template template, Long id) {
        Template templateDb = findById(id);
        templateDb.setName(template.getName());
        templateDb.setSubject(template.getSubject());
        templateDb.setBody(template.getBody());
        return templateDb;
    }

    @Override
    public void deleteById(Long id) {
    templateRepository.deleteById(id);
    }
}
