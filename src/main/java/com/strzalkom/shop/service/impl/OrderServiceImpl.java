package com.strzalkom.shop.service.impl;

import com.strzalkom.shop.model.dao.*;
import com.strzalkom.shop.repository.BasketRepository;
import com.strzalkom.shop.repository.OrderRepository;
import com.strzalkom.shop.service.OrderService;
import com.strzalkom.shop.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
@RequiredArgsConstructor
public class OrderServiceImpl implements OrderService {


    private final OrderRepository orderRepository;
    private final BasketRepository basketRepository;
    private final UserService userService;


    @Override
    public List<Product> getOrderProducts(Long orderId) {

        return orderRepository.findByOrderDetailsId(orderId).stream()
                .map(order -> order.getProduct().toBuilder()
                        .name(order.getProduct().getName())
                        .description(order.getProduct().getDescription())
                        .quantity(order.getProduct().getQuantity())
                        .price(order.getProduct().getPrice())
                        .build())
                .collect(Collectors.toList());
    }

    @Override
    public List<Product> getProductListByUser(Long userId){
            User userDb = userService.findById(userId);


            return orderRepository.findByOrderDetailsUserId(userDb.getId()).stream()
                    .map(order -> {
                        Product basketProduct = order.getProduct();
                        basketProduct.setQuantity(order.getQuantity());
                        return basketProduct;
                    }) //jak zrobic zeby napisac wiecej niz jedna linijke w lambdzie
                    .collect(Collectors.toList());

    }

        @Override
        public List<Product> getProductListByOrder(Long orderId){
//zminic nazewnictwo
           return orderRepository.findByOrderDetailsId(orderId).stream()
                    .map(order -> {
                        Product orderProduct = order.getProduct();
                        orderProduct.setQuantity(order.getQuantity());
//                        orderProduct.getQuantity(basketRepository.findByUserIdAndProductId(userDb.getId(), orderProduct.getId()).stream().map(Basket::getQuantity));
//                        product.setQuantity(basketRepository.findByUserIdAndProductId(userDb.getId(), product.getId()).orElseThrow(() -> new EntityNotFoundException("User not logged in")));
                        return orderProduct;
                    })
                    .collect(Collectors.toList());
        }


    @Override
    public void saveOrder(List<Order> orderList) {
        orderRepository.saveAll(orderList);
    }

    @Override
    public Order getById(Long id) {
        return orderRepository.getReferenceById(id);
    }

    @Override
    public List<Order> getByOrderDetailsId(Long id){
        return  orderRepository.findByOrderDetailsId(id);
    }




}
