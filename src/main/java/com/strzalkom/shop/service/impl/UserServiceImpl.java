package com.strzalkom.shop.service.impl;

import com.strzalkom.shop.model.dao.User;
import com.strzalkom.shop.repository.RoleRepository;
import com.strzalkom.shop.repository.UserRepository;
import com.strzalkom.shop.security.SecurityUtils;
import com.strzalkom.shop.service.EmailService;
import com.strzalkom.shop.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
//@Repository, @Configuration, @RestController, @Controller, @Service,@Component ->(wszystkie poprzednie dziedziczą), @Bean (w przypadku obiektów zwracanych z metod)


@Service //adnotacja umożliwia rejestrowanie klasy jako bean w springu
@RequiredArgsConstructor   //tworzy konstruktor wieloargumentowy dla wszystkich zmiennych finalnych
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final PasswordEncoder passwordEncoder;

    private final EmailService emailService;


    @Override
    public User save(User user) {

        roleRepository.findByName("USER").ifPresent(role -> user.setRoles(Collections.singletonList(role))); //zwracanie jednoelementowej listy (singleton)

        user.setActivatedToken(UUID.randomUUID());
        user.setPassword(passwordEncoder.encode(user.getPassword()));
         userRepository.save(user);
         Map<String, Object> variables = new HashMap<>();
        variables.put("firstName", user.getFirstName());
        variables.put("lastName", user.getLastName());
        variables.put("url", "http://localhost:8096/api/users/activate?activatedToken=" + user.getActivatedToken());
//endpoint controller jak podac uuid

         emailService.sendMail(user.getEmail(), "EmailConfirmation", variables);
         return user;
    }


    @Override
    @Transactional
    //sprawdza czy stara wartosc z db oraz nowa wartosc sa od siebie rozne -> jezeli prawda to wykonuje update
    public User update(User user, Long id) {
        User userDb = findById(id);
        userDb.setEmail(user.getEmail());
        userDb.setFirstName(user.getFirstName());
        userDb.setLastName(user.getLastName());

        return userDb;
    }

    @Override
    public User findById(Long id) {
        return userRepository.getById(id);
    }

    @Override
    public void deleteById(Long id) {
        userRepository.deleteById(id);
    }

    @Override
    public Page<User> getPage(Pageable pageable) {
        return userRepository.findAll(pageable);
    }

    @Override
    @Transactional
    public void activateUser(UUID activatedToken) {

        userRepository.findByActivatedToken(activatedToken).ifPresent(user -> {
            user.setActivatedToken(null);
            userRepository.save(user);
        });

    }

    @Override
    public User findCurrentUser() {

        return userRepository.findByEmail(SecurityUtils.getCurrentUserEmail())
                .orElseThrow(() -> new EntityNotFoundException("User not logged in"));  //supplier
    }
}
