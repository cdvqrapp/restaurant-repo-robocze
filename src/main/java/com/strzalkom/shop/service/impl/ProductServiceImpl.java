package com.strzalkom.shop.service.impl;

import com.strzalkom.shop.config.properties.FileConfig;
import com.strzalkom.shop.helper.FileHelper;
import com.strzalkom.shop.model.dao.Product;

import com.strzalkom.shop.repository.ProductRepository;

import com.strzalkom.shop.service.ProductService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FilenameUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.transaction.Transactional;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.List;

@Slf4j
@Service
@RequiredArgsConstructor
public class ProductServiceImpl implements ProductService {

    private final ProductRepository productRepository;
    private final FileConfig fileConfig;

    private final FileHelper fileHelper;


    @Override
    @Transactional //czeka na wykonanie calej funkcji zeby zapisac do bazy danych
    public Product save(Product product, MultipartFile image) {
        productRepository.save(product);
        String extension = FilenameUtils.getExtension(image.getOriginalFilename());
        Path path = Paths.get(fileConfig.getProductPath(), product.getId() + "." + extension);
        String imagePath = path.toString();
        try {
            fileHelper.saveFile(image.getInputStream(), imagePath);
            product.setImagePath(imagePath);
        } catch (IOException e) {
            log.error("Can not save image.", e);
        }
        return product;
    }


    @Override
    @Transactional
    //sprawdza czy stara wartosc z db oraz nowa wartosc sa od siebie rozne -> jezeli prawda to wykonuje update
    public Product update(Product product, Long id, MultipartFile image) {
        Product productDb = findById(id);
        productDb.setName(product.getName());
        productDb.setQuantity(product.getQuantity());
        productDb.setPrice(product.getPrice());
        productDb.setDescription(product.getDescription());
        productDb.setCode(product.getCode());
        String oldPath = productDb.getImagePath();

        String extension = FilenameUtils.getExtension(image.getOriginalFilename());
        Path path = Paths.get(fileConfig.getProductPath(), productDb.getId() + "." + extension);
        String imagePath = path.toString();
        try {
            fileHelper.saveFile(image.getInputStream(), imagePath);
            productDb.setImagePath(imagePath);

            if (oldPath != null && !productDb.getImagePath().equals(oldPath))
                fileHelper.deleteFile(oldPath);


        } catch (IOException e) {
            log.error("Can not save image.", e);
        }

        return productDb;
    }

    @Override
    public Product findById(Long id) {
        return productRepository.getById(id);
    }

   @Override
    public List<Product> findByIds(List<Long> ids){
        return productRepository.findByIdIn(ids);
    }
    @Override
    public void deleteById(Long id) {
        Product productDb = findById(id);
        try {
            String imagePath = productDb.getImagePath();
            fileHelper.deleteFile(imagePath);
        } catch (IOException e) {
            log.error("Image path doesnt exist", e);
        }
        productRepository.deleteById(id);
    }

    @Override
    public Page<Product> getProductsByCategory(Long categoryId, Pageable pageable) {
        return productRepository.findByCategoryId(categoryId, pageable);
    }

    @Override
    public Page<Product> getPage(Pageable pageable) {
        return productRepository.findAll(pageable);
    }
}


// hasło 3 duze 3 małe do mysql

//DTO - komunikacja z klientem
//DAO - komunikacja z bazą danych - to jest DAO