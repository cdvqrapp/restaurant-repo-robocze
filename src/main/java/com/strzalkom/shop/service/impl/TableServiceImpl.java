package com.strzalkom.shop.service.impl;

import com.strzalkom.shop.model.dao.TableObject;
import com.strzalkom.shop.repository.TableRepository;
import com.strzalkom.shop.service.TableService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import javax.persistence.NonUniqueResultException;
import javax.transaction.Transactional;

@Service
@RequiredArgsConstructor
public class TableServiceImpl implements TableService {

    private final TableRepository tableRepository;


    @Override
    @Transactional
    public TableObject save(TableObject tableObject)  {
        Long tableNumber =tableObject.getTableNumber();
       if(tableRepository.findByTableNumber(tableNumber).isPresent()) {
               throw new NonUniqueResultException("There is table with this number  " + tableNumber);
       }else {
           tableRepository.save(tableObject);
       }
        return tableObject;
    }

    @Override
    @Transactional
    public TableObject update(TableObject tableObject, Long id) {
        TableObject tableObjectDb = findById(id);
        tableObjectDb.setTableNumber(tableObject.getTableNumber());
        tableObjectDb.setDescription(tableObject.getDescription());
        tableObjectDb.setGuestCounter(tableObject.getGuestCounter());
        return tableObjectDb;
    }

    @Override
    public TableObject findById(Long id) {

        return tableRepository.getReferenceById(id);
    }

    @Override
    public TableObject findByNumber(Long tableNumber){
        return tableRepository.findByTableNumber(tableNumber)
                .orElseThrow(() -> new EntityNotFoundException("TableObject number: " + tableNumber)) ;
    }

    @Override
    public void deleteById(Long id) {
        tableRepository.deleteById(id);

    }

    @Override
    public TableObject findCurrentTableByOrder() {
        return null;
    }

    @Override
    public Page<TableObject> getPage(Pageable pageable) {
        return tableRepository.findAll(pageable);
    }
}
