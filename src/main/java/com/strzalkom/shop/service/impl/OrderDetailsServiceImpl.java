package com.strzalkom.shop.service.impl;

import com.strzalkom.shop.model.OrderStatus;
import com.strzalkom.shop.model.PaymentMethod;
import com.strzalkom.shop.model.dao.*;
import com.strzalkom.shop.model.dto.OrderDetailsDto;
import com.strzalkom.shop.repository.OrderDetailsRepository;
import com.strzalkom.shop.repository.ProductRepository;
import com.strzalkom.shop.service.*;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor //konstruktor wieloargumentow ydlaa finalnych zmiennych
public class OrderDetailsServiceImpl implements OrderDetailsService {

    private final OrderDetailsRepository orderDetailsRepository;
    private final BasketService basketService;
    private final UserService userService;
    private final OrderService orderService;

    private final TableService tableService;

    private final ProductService productService;




//    @Override
//    @Transactional
//// w przypadku w ktorym pobieramy obiekt z db a nastepnie zmieniamy go nastepuje update obiektu (jezeli bedziemy ustawiac inny status niz w db)
//    public void changeOrderStatus(Long orderId, OrderStatus orderStatus) {
//        orderDetailsRepository.getById(orderId).setOrderStatus(orderStatus);
//
//
//    }

    @Override
    @Transactional
    public void changeTableIdInOrder(Long tableNumber, Long orderId){
        orderDetailsRepository.getReferenceById(orderId).setTableObject(tableService.findByNumber(tableNumber));
    }

    @Override
    public OrderDetails getById(Long id) {
        return orderDetailsRepository.getReferenceById(id);
    }


    @Override
    @Transactional
    public void changeOrderStatus(Long orderId, OrderStatus orderStatus){
        orderDetailsRepository.getReferenceById(orderId).setOrderStatus(orderStatus);
    }

    @Override
    @Transactional
    public void changeOrderPaymentMethod(Long orderId, PaymentMethod paymentMethod) {
        orderDetailsRepository.getReferenceById(orderId).setPaymentMethod(paymentMethod);
    }
//osobna metoda createOrderWithTable
    @Override
    public void createOrderWithTable(String description, Long tableId, List<Product> productList){
        User currentUser = userService.findCurrentUser();
        TableObject tableObject = tableService.findById(tableId);
        OrderDetails orderDetails = orderDetailsRepository.save(OrderDetails.builder()
                .user(currentUser)
                .orderStatus(OrderStatus.PREPARATION)
                .paymentMethod(PaymentMethod.CARD)
                .clientDescription(description)
                .tableObject(tableObject)
                .build());

        List<Product> products = productService.findByIds(productList.stream()
                .map(Product::getId)
                .collect(Collectors.toList()));


        //zamiana na mapę

        Map<Long, Integer> idToQuantityMap = productList.stream()
                .collect(Collectors.toMap(Product::getId, Product::getQuantity));

        List<Order> orderList = products.stream()
                .map(product -> {
                    Integer quantity = idToQuantityMap.get(product.getId());
                    if (quantity > product.getQuantity()) {
                        quantity = product.getQuantity();
                    }
                    return Order.builder()
                            .product(product)
                            .orderDetails(orderDetails)
                            .quantity(quantity)
                            .build();
                })
                .collect(Collectors.toList());

        orderService.saveOrder(orderList);


        //jak w strumieniu przy uzyciu terminalnego operatora stworzyc mape
        //przeiterowac po tym co productService
        //czy quantity jest mniejsze lub rowne quantity z mapy

    }

    @Override
    @Transactional
    public void createOrder(String description, Long tableNumber) {
        User currentUser = userService.findCurrentUser();
        TableObject tableObject = null;
        if(tableNumber != null) {
//             tableObject = tableService.findById(tableId);
             tableObject =tableService.findByNumber(tableNumber);
        }
        OrderDetails orderDetails = orderDetailsRepository.save(OrderDetails.builder()
                .user(currentUser)
                .orderStatus(OrderStatus.PREPARATION)
                .paymentMethod(PaymentMethod.CARD)
                .clientDescription(description)
                .tableObject(tableObject)
                .build());

        List<Order> productsOrder = basketService.getUserBasket(currentUser.getId()).stream()
                .map(basket -> Order.builder()
                        .product(basket.getProduct())
                        .orderDetails(orderDetails)
                        .unitPrice(basket.getProduct().getPrice())
                        .quantity(basket.getQuantity())
                        .build())
                .collect(Collectors.toList());

        orderDetails.setTotalPrice(productsOrder.stream()
                .mapToLong(order -> order.getUnitPrice() * order.getQuantity())
                .sum());

        orderService.saveOrder(productsOrder);
        basketService.clearBasket();

    }


    @Override
    public void deleteOrder(OrderDetails order) {
        order.setOrderStatus(OrderStatus.REJECTED);
        orderDetailsRepository.deleteById(order.getId());
    }


    @Override
    public Page<OrderDetails> getPage(Pageable pageable) {
        return orderDetailsRepository.findByUserId(userService.findCurrentUser().getId(), pageable);
    }

    @Override
    public Page<OrderDetails> getAll(Pageable pageable) {
        return orderDetailsRepository.findAll(pageable);
    }


    //napisac metode do skladania zamowienia

    //preparing na sztywno
    //przeniesc z basketu do zamowienia + czyszczenie koszyka
}
