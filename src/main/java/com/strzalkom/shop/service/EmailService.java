package com.strzalkom.shop.service;

import java.util.Map;

public interface EmailService {

    void sendMail(String email, String templateName, Map<String, Object> variables);
}
