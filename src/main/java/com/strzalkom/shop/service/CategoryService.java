package com.strzalkom.shop.service;

import com.strzalkom.shop.model.dao.Category;
import com.strzalkom.shop.model.dao.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface CategoryService {

    Category save(Category category);

    Category update(Category category, Long id);

    void deleteById(Long id);

    Category findById(Long id);

    Page<Category> getPage(Pageable pageable);



}
