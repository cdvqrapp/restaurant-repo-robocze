package com.strzalkom.shop.service;

import com.strzalkom.shop.model.dao.Product;
import com.strzalkom.shop.model.dao.Reservation;
import com.strzalkom.shop.model.dao.TableObject;
import com.strzalkom.shop.model.dao.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface ReservationService {

    Reservation save(Reservation reservation);

    Reservation update(Long id, Reservation reservation);

    void deleteById(Long id);

    Reservation findById(Long id);


    List<Reservation> findByTable(Long tableId);

    Page<Reservation> getPage(Pageable pageable);
}
