package com.strzalkom.shop.service;

import com.strzalkom.shop.model.dao.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.UUID;

public interface UserService {

    User save(User user);

    User update(User user, Long id);

    User findById(Long id);

    void deleteById(Long id);

    User findCurrentUser();

    Page<User> getPage(Pageable pageable);

    void activateUser(UUID activatedToken);
}
