package com.strzalkom.shop.service;

import com.strzalkom.shop.model.dao.Template;

public interface TemplateService {

    Template findByName(String name);

    Template save(Template template);

    Template update(Template template, Long id);

    Template findById(Long id);
    void deleteById(Long id);

}
