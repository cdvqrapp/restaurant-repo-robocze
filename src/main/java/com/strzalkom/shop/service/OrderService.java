package com.strzalkom.shop.service;

import com.strzalkom.shop.model.dao.Order;
import com.strzalkom.shop.model.dao.Product;

import java.util.List;

public interface OrderService {


    List<Product> getOrderProducts(Long orderId);

    void saveOrder(List<Order> orderList);

    Order getById(Long id);

    List<Order> getByOrderDetailsId(Long id);

     List<Product> getProductListByUser(Long userId);

    public List<Product> getProductListByOrder(Long orderId);



}
